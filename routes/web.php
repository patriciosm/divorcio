<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

//Rutas de sistema de administración
Route::prefix('admin')->group(function() {
	
	Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::post('logout' , 'Auth\AdminLoginController@logout')->name('admin.logout');

	//Rutas del panel de administración
	Route::middleware('auth:admin')->group(function() {

		Route::get('/', 'AdministradorController@index')->name('admin.dashboard');
	
		Route::prefix('contenidos')->group(function() {

			Route::get('/', 'ContenidoController@showContenidos')->name('lista-contenidos');
			Route::get('edit/{id}', 'ContenidoController@editContenidos')->name('edita-contenido');
			Route::post('edit', 'ContenidoController@updateContenidos')->name('update-contenido');
			Route::get('tarifa/edit/{id}', 'ContenidoController@editTarifa')->name('edita-tarifa');
			Route::post('tarifa/edit', 'ContenidoController@updateTarifa')->name('update-tarifa');
		});

		Route::prefix('casos')->group(function() {

			Route::get('/', 'CasoController@listarCasos')->name('lista-casos');
			Route::get('{id}', 'CasoController@showCaso')->name('ver-caso');
			Route::get('edit/{id}', 'CasoController@editCaso')->name('edit-caso');
			Route::post('edit', 'CasoController@updateCaso')->name('update-caso');
		
		});

	});

});

Route::middleware('auth')->group(function() {

	// Protección de rutas
	Route::get('/login')->name('login');
	Route::get('/register')->name('register');
	Route::get('/logout')->name('logout');
	Route::get('/password/email')->name('password.email');
	Route::get('/password/reset')->name('password.request');

	Route::prefix('formulario')->group(function() {

		Route::middleware('caso')->group(function() {

			Route::get('paso1' , 'DemandaController@showMatrimonioForm')->name('form.matrimonio');
			Route::post('paso1' , 'DemandaController@saveMatrimonioForm')->name('form.matrimonio.save');
			Route::get('paso2' , 'DemandaController@showConyugesForm')->name('form.conyuges');
			Route::post('paso2' , 'DemandaController@saveConyugesForm')->name('form.conyuges.save');
			Route::get('paso3' , 'DemandaController@showHijosForm')->name('form.hijos');
			Route::post('paso3' , 'DemandaController@saveHijosForm')->name('form.hijos.save');
			Route::get('paso4' , 'DemandaController@showBienesForm')->name('form.bienes');
			Route::post('paso4' , 'DemandaController@saveBienesForm')->name('form.bienes.save');
			Route::get('paso5' , 'DemandaController@showVisitasForm')->name('form.visitas');
			Route::post('paso5' , 'DemandaController@saveVisitasForm')->name('form.visitas.save');
			Route::get('final' , 'DemandaController@showPasoFinal')->name('paso.final');
			Route::get('comuna/{idRegion}' , 'ComunasController@listarComunas');

		});
		
		Route::post('final' , 'DemandaController@savePasoFinal')->name('paso.final.save');

	});

	Route::get('dashboard' , 'DemandaController@index')->name('dashboard');

});

Route::get('auth/{provider}' , 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
Route::get('auth/{provider}/callback' , 'Auth\SocialAuthController@handleProviderCallback');


// Rutas de autorización y registro de usuarios
Route::post('/login' , 'Auth\LoginController@login')->name('login');
Route::post('/register' , 'Auth\RegisterController@register')->name('register');
Route::post('/logout' , 'Auth\LoginController@logout')->name('logout');
Route::post('/password/email' , 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('/password/reset' , 'Auth\ResetPasswordController@reset')->name('password.request');
Route::get('/password/reset/{token}' , 'Auth\ResetPasswordController@showResetForm')->name('password.reset');