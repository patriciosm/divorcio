<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{
    protected $table = "visitas";
    protected $fillable = [
                'idMatrimonio' ,
                'idTipovisita' ,
                'otraVisita' ,
                'horaRetiro' ,
                'horaRestitucion' ,
                'fechaInicioVerano' ,
                'fechaFinVerano' ,
                'otroVerano' ,
                'diasInvierno' ,
                'cumpleanos' ,
                'fiestasPatrias' ,
                'diaFestejado' ,
                'cumplePadres' ,
                'diaPadreMadre' 
                ];

    //Relación inversa "uno a uno" con modelo Matrimonio
    public function matrimonio()
    {
    	return $this->belongsTo('App\Models\Matrimonio','idMatrimonio');
    }

    //Relación inversa 'uno a muchos' con modelo Tipovisita
    public function tipovisita()
    {
    	return $this->belongsTo('App\Models\Tipovisita','idTipovisita');
    }

}
