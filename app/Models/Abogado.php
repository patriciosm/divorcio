<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Abogado extends Model
{
    protected $table = "abogados";
    protected $fillable = ['nombre' , 'apellido'];

    //Relación 'uno a muchos' con modelo Caso
    public function casos()
    {
    	return $this->hasMany('App\Models\Caso','idAbogado');
    }

    //Une campos de nombre y apellido para mostrarlos en las vistas
    public function getNombreCompletoAttribute()
    {
    	return $this->nombre . " " . $this->apellido;
    }
}
