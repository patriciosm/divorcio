<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bien extends Model
{
    protected $table = "bienes";
    protected $fillable = ['idMatrimonio' , 'nombre' , 'direccion' , 'valor'];

    //Relación inversa 'uno a muchos' con modelo Matrimonio
    public function matrimonio()
    {
    	return $this->belongsTo('App\Models\Matrimonio','idMatrimonio');
    }

}
