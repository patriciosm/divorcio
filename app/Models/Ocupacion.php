<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ocupacion extends Model
{
    protected $table = "ocupaciones";
    protected $fillable = ['ocupacion'];

    //Relación "uno a muchos" con modelo Hijo
    public function hijos()
    {
    	return $this->hasMany('App\Models\Hijo','idOcupacion');
    }
}
