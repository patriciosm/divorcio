<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contenido extends Model
{
    protected $table = "contenidos";
    protected $fillable = ['idSeccion' , 'idTipodato' , 'titulo' , 'subtitulo' , 'texto'];

    //Relación inversa 'uno a muchos' con modelo Seccion
    public function seccion()
    {
    	return $this->belongsTo('App\Models\Seccion' , 'idSeccion');
    }

    //Relación inversa 'uno a muchos' con modelo Tipodato
    public function tipoDato()
    {
    	return $this->belongsTo('App\Models\Tipodato' , 'idTipodato');
    }
}
