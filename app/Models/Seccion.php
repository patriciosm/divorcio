<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seccion extends Model
{
    protected $table = "secciones";
    protected $fillable = ['seccion'];
    
    //Relación "uno a muchos" con modelo Contenido
    public function contenidos()
    {
    	return $this->hasMany('App\Models\Contenido' , 'idSeccion');
    }
}
