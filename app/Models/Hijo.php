<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hijo extends Model
{
    protected $table = "hijos";
    protected $fillable = [
                'idMatrimonio' ,
                'nombres' ,
                'apellidos' ,
                'edad' ,
                'rut' ,
                'idOcupacion' ,
                'idTuicion' ,
                'lugar'
                ];

    //Relación inversa 'uno a muchos' con modelo Matrimonio
    public function matrimonio()
    {
    	return $this->belongsTo('App\Models\Matrimonio' , 'idMatrimonio');
    }

    //Relación inversa 'uno a muchos' con modelo Ocupacion
    public function ocupacion()
    {
    	return $this->belongsTo('App\Models\Ocupacion','idOcupacion');
    }

    //Relación inversa 'uno a muchos' con modelo Tuicion
    public function tuicion()
    {
    	return $this->belongsTo('App\Models\Tuicion','idTuicion');
    }
}
