<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    
	protected $table = "pagos";
	protected $fillable = ['idCaso' , 'valor' , 'fecha' , 'idFormapago'];

    //Relación inversa "uno a muchos" con modelo Formapago
    public function forma()
    {
    	return $this->belongsTo('App\Models\FormaPago' , 'idFormapago');
    }

    //Relación inversa "uno a muchos" con modelo Caso
    public function caso()
    {
    	return $this->belongsTo('App\Models\Caso' , 'idCaso');
    }

}
