<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipodato extends Model
{
    protected $table = "tiposdatos";
    protected $fillable = ['tipo'];

    //Relación "uno a muchos" con modelo Contenido
    public function contenidos()
    {
    	return $this->hasMany('App\Models\Contenido' , 'idTipodato');	
    }
}
