<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Formapago extends Model
{
    protected $table = "formaspagos";
    protected $fillable = ['formapago'];

	//Relación 'uno a muchos' con modelo Pago
    public function pagos()
    {
    	return $this->hasMany('App\Models\Pago' , 'idFormapago');
    }
}
