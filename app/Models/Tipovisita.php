<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipovisita extends Model
{
    protected $table = "tiposvisitas";
    protected $fillable = ['tipo'];

    //Relación "uno a muchos" con modelo Visita
    public function visitas()
    {
    	return $this->hasMany('App\Models\Visita','idTipovisita');
    }
}
