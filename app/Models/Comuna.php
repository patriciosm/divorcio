<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comuna extends Model
{
    protected $table = "comunas";
    protected $fillable = ['idRegion' , 'comuna'];

    //Relación inversa 'uno a muchos' con modelo Region 
    public function region()
    {
    	return $this->belongsTo('App\Models\Region' , 'idRegion');
    }
}
