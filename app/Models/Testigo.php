<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testigo extends Model
{
    protected $table = "testigos";
    protected $fillable = ['idCaso' , 'nombre' , 'apellido' , 'rut'];

    //Relación inversa "uno a muchos" con modelo Caso
    public function caso(){
    	return $this->belongsTo('App\Models\Caso' , 'idCaso');
    }
}
