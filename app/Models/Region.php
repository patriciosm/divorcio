<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = "regiones";
    protected $fillable = ['region'];

    //Relación 'uno a muchos' con modelo Comuna
    public function comunas()
    {
    	return $this->hasMany('App\Models\Comuna' , 'idRegion');
    }
}
