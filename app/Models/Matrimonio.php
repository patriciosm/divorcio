<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matrimonio extends Model
{
    
    protected $table = "matrimonios";
    protected $fillable = [
    			'idUser' ,
    			'fechaMatrimonio' ,
    			'ceseConvivencia' ,
    			'dejaHogar' ,
    			'renunciaCompensacion' ,
    			'favorCompensacion' ,
    			'montoCompensacion' ,
                'tieneBienes' ,
    			'nombresMarido' ,
    			'apellidosMarido' ,
    			'rutMarido' ,
    			'ocupacionMarido' ,
                'domicilioMarido' ,
    			'idComunaMarido' ,
    			'emailMarido' ,
    			'telefonoMarido' ,
    			'nombresMujer' ,
    			'apellidosMujer' ,
    			'rutMujer' ,
    			'ocupacionMujer' ,
                'domicilioMujer' ,
    			'idComunaMujer' ,
    			'emailMujer' ,
    			'telefonoMujer' ,
    			'cantidadHijos',
                'montoPension' ,
                'estado'
    			];

    public function user()
    {
        return $this->belongsTo('App\Models\User' , 'idUser');
    }

    //Relación 'uno a muchos' con modelo Hijo
    public function hijos()
    {
    	return $this->hasMany('App\Models\Hijo','idMatrimonio');
    }

    //Relación 'uno a uno' con modelo Caso
    public function caso()
    {
    	return $this->hasOne('App\Models\Caso','idMatrimonio');
    }

    //Relación 'pertenece a' con modelo Comuna 
    public function comunaMarido()
    {
        return $this->belongsTo('App\Models\Comuna' , 'idComunaMarido')->withDefault(['idRegion' => '']);
    }

    //Relación 'pertenece a' con modelo Comuna
    public function comunaMujer()
    {
        return $this->belongsTo('App\Models\Comuna' , 'idComunaMujer')->withDefault(['idRegion' => '']);
    }

    //Relación 'uno a muchos' con modelo Bien
    public function bienes()
    {
        return $this->hasMany('App\Models\Bien','idMatrimonio');
    }

    //Relación 'uno a uno' con modelo Visita
    public function visita()
    {
        return $this->hasOne('App\Models\Visita','idMatrimonio')->withDefault(['otraVisita' => '']);
    }

}
