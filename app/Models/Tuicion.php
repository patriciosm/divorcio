<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tuicion extends Model
{
    protected $table = "tuiciones";
    protected $fillable = ['tutor'];

    //Relación "uno a muchos" con modelo Hijo
    public function hijos()
    {
    	return $this->hasMany('App\Models\Hijo','idTuicion');
    }
}
