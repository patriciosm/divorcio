<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Horaaudiencia extends Model
{
    
    protected $table = "horasaudiencias";
    protected $fillable = ['idCaso' , 'fecha' , 'hora'];

    //Relación inversa "uno a muchos" con modelo Caso
    public function caso()
    {
    	return $this->belongsTo('App\Models\Caso' , 'idCaso');
    }

}
