<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Caso extends Model
{
    protected $table = "casos";
    protected $fillable = ['idMatrimonio' , 'idEstado' , 'idAbogado'];

    //Relación inversa 'uno a uno' con modelo Matrimonio
    public function matrimonio()
    {
		return $this->belongsTo('App\Models\Matrimonio' , 'idMatrimonio');
    }

    //Relación inversa 'uno a muchos' con modelo Estado
    public function estado()
    {
		return $this->belongsTo('App\Models\Estado' , 'idEstado');
    }

    //Relación inversa 'uno a muchos' con modelo Abogado
    public function abogado()
    {
		return $this->belongsTo('App\Models\Abogado' , 'idAbogado')->withDefault(['nombreCompleto' => '']);
    }

    //Relación 'uno a muchos' con modelo Pago
    public function pagos()
    {
        return $this->hasMany('App\Models\Pago','idCaso');
    }

    //Relación 'uno a muchos' con modelo Horatomada
    public function horasTomadas()
    {
        return $this->hasMany('App\Models\Horatomada','idCaso');
    }

    //Selecciona la última fecha y hora tomada
    public function getUltimaHoraTomadaAttribute()
    {
        $consulta = $this->horastomadas()->orderBy('fecha' , 'DESC')->first();
        $dato = "No hay hora tomada";
        if(!empty($consulta))
        {
            $dato = formatoFecha($consulta->fecha) . " " . $consulta->hora;
        }
        return $dato;
    }
    
    //Relación 'uno a muchos' con modelo Horaaudiencia
    public function horasAudiencias()
    {
        return $this->hasMany('App\Models\Horaaudiencia','idCaso'); 
    }

    //Selecciona la última fecha y hora audiencia
    public function getUltimaHoraAudienciaAttribute()
    {
        $consulta = $this->horasAudiencias()->orderBy('fecha' , 'DESC')->first();
        $dato = "No hay hora de audiencia";
        if(!empty($consulta))
        {
            $dato = formatoFecha($consulta->fecha) . " " . $consulta->hora;
        }
        return $dato;
    }

    //Relación 'uno a muchos' con modelo Testigo
    public function testigos()
    {
        return $this->hasMany('App\Models\Testigo','idCaso');
    }
}
