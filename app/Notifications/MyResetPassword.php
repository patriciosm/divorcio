<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Auth\Notifications\ResetPassword;
// use Illuminate\Notifications\Notification;
// use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MyResetPassword extends ResetPassword
{
    use Queueable;

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Restablecer contraseña de Divorcio en Linea')
                    ->greeting('Olvidaste tu contraseña')
                    ->line('Para cambiar tu contraseña de Divorcio en linea has click en el siguiente boton')
                    ->action('Restablecer contraseña', route('password.reset', $this->token))
                    ->line('Gracias por utilizar a Divorcioenlinea.cl')
                    ->salutation('Saludos.');;
    }

}
