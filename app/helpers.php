<?php

//Formato de miles de pesos
function formatoMiles($value)
{
	return number_format($value,0,',','.');
}

//Formatea fecha de 'aaaa-mm-dd' a 'dd-mm-aaaa' 
function formatoFecha($value)
{
	return date('d-m-Y',strtotime($value));
}