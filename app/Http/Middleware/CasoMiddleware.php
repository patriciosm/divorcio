<?php

namespace App\Http\Middleware;

use Closure;

class CasoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->matrimonio->estado == 0)
        {
            return redirect()->route('dashboard');
        }
        return $next($request);
    }
}
