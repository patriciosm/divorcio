<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Matrimonio;
use App\Models\Region;
use App\Models\Comuna;
use App\Models\Ocupacion;
use App\Models\Tuicion;
use App\Models\Tipovisita;
use App\Models\Horatomada;
use App\Models\Horaaudiencia;
use App\Models\Hijo;
use App\Models\Bien;
use App\Models\Visita;
use App\Models\Caso;
use PDF;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

class DemandaController extends Controller
{
    
	public function index()
	{
		$id = Auth::id();
		$matrimonio = Matrimonio::firstOrCreate(['idUser' => $id]);
		$caso = $matrimonio->caso;
		if($caso)
		{
			return view('users.dashboard')->with('caso' , $caso);
		}
		else
		{
			return redirect()->route('form.matrimonio');
		}
	}

	public function showMatrimonioForm()
	{
		$user = Auth::user();
		$matrimonio = $user->matrimonio;
		return view('users.forms.form-matrimonio')->with('matrimonio' , $matrimonio);
	}

	public function saveMatrimonioForm(Request $request)
	{
		$user = Auth::user();
		$matrimonio = $user->matrimonio;
		$matrimonio->fechaMatrimonio = $request->input('fechaMatrimonio');
		$matrimonio->ceseConvivencia = $request->input('ceseConvivencia');
		$matrimonio->dejaHogar = $request->input('dejaHogar');
		$matrimonio->renunciaCompensacion = $request->input('renunciaCompensacion');
		$matrimonio->favorCompensacion = $request->input('favorCompensacion');
		$matrimonio->montoCompensacion = ($request->input('montoCompensacion') == '') ? 0 : $request->input('montoCompensacion');
		$matrimonio->save();
		return redirect()->route('form.conyuges');
	}

	public function showConyugesForm()
	{
		$user = Auth::user();
		$matrimonio = $user->matrimonio;
		$comunas = Comuna::all()->pluck('comuna' , 'id');
		$regiones = Region::all()->pluck('region' , 'id');
		return view('users.forms.form-conyuges' , ['matrimonio' => $matrimonio , 'comunas' => $comunas , 'regiones' => $regiones] );	
	}

	public function saveConyugesForm(Request $request)
	{
		$user = Auth::user();
		$matrimonio = $user->matrimonio;
		$matrimonio->nombresMarido = $request->input('nombresMarido');
		$matrimonio->apellidosMarido = $request->input('apellidosMarido');
		$matrimonio->rutMarido = $request->input('rutMarido');
		$matrimonio->ocupacionMarido = $request->input('ocupacionMarido');
		$matrimonio->domicilioMarido = $request->input('domicilioMarido');
		$matrimonio->idComunaMarido = $request->input('idComunaMarido');
		$matrimonio->emailMarido = $request->input('emailMarido');
		$matrimonio->telefonoMarido = $request->input('telefonoMarido');
		$matrimonio->nombresMujer = $request->input('nombresMujer');
		$matrimonio->apellidosMujer = $request->input('apellidosMujer');
		$matrimonio->rutMujer = $request->input('rutMujer');
		$matrimonio->ocupacionMujer = $request->input('ocupacionMujer');
		$matrimonio->domicilioMujer = $request->input('domicilioMujer');
		$matrimonio->idComunaMujer = $request->input('idComunaMujer');
		$matrimonio->emailMujer = $request->input('emailMujer');
		$matrimonio->telefonoMujer = $request->input('telefonoMujer');
		$matrimonio->cantidadHijos = $request->input('cantidadHijos');
		$matrimonio->save();
		return redirect()->route('form.hijos');
	}

	public function showHijosForm()
	{
		$id = Auth::id();
		$matrimonio = Matrimonio::where('idUser', $id)->select('id' , 'montoPension')->firstOrFail();
		$hijos = Hijo::where('idMatrimonio' , $matrimonio->id);
		if($hijos->count() > 0)
		{
			$hijos = $hijos->get();
		}
		else
		{
			$hijos = "";
		}
		$ocupaciones = Ocupacion::all()->pluck('ocupacion' , 'id');
		$tuiciones = Tuicion::all();
		return view('users.forms.form-hijos', ['matrimonio' => $matrimonio , 'hijos' => $hijos , 'ocupaciones' => $ocupaciones , 'tuiciones' => $tuiciones]);
	}

	public function saveHijosForm(Request $request)
	{
		$id = Auth::id();
		$matrimonio = Matrimonio::where('idUser', $id)->select('id' , 'montoPension')->firstOrFail();
		foreach ($request->input('exp') as $req) :
			if(isset($req['id']))
			{
				$hijo = Hijo::find($req['id']);
				$hijo->fill($req);
				$hijo->idMatrimonio = $matrimonio->id;
				$hijo->save();
			}
			else
			{
				if($req['nombres'] != '' || $req['apellidos'] != '')
				{
					$hijo = new Hijo;
					$hijo->fill($req);
					$hijo->idMatrimonio = $matrimonio->id;
					$hijo->save();
				}
			}
		endforeach;
		$matrimonio->montoPension = $request->input('montoPension');
		$matrimonio->save();
		return redirect()->route('form.bienes');
	}

	public function showBienesForm()
	{
		$id = Auth::id();
		$matrimonio = Matrimonio::where('idUser', $id)->select('id' , 'tieneBienes')->firstOrFail();
		$bienes = ($matrimonio->bienes->isEmpty()) ? "" : $matrimonio->bienes;
		return view('users.forms.form-bienes', ['bienes' => $bienes , 'matrimonio' => $matrimonio]);
	}	

	public function saveBienesForm(Request $request)
	{
		$id = Auth::id();
		$matrimonio = Matrimonio::where('idUser', $id)->firstOrFail();
		$matrimonio->tieneBienes = $request->input('tieneBienes');
		foreach ($request->input('exp') as $req) :
			if(isset($req['id']))
			{
				$bien = Bien::find($req['id']);
				$bien->fill($req);
				$bien->idMatrimonio = $matrimonio->id;
				$matrimonio->tieneBienes = 1;
				$bien->save();
			}
			else
			{
				if($req['nombre'] != '')
				{
					$bien = new Bien;
					$bien->fill($req);
					$bien->idMatrimonio = $matrimonio->id;
					$matrimonio->tieneBienes = 1;
					$bien->save();
				}
			}
		endforeach;

		$matrimonio->save();
		return redirect()->route('form.visitas');
	}

	public function showVisitasForm()
	{
		$id = Auth::id();
		$matrimonio = Matrimonio::where('idUser', $id)->select('id')->firstOrFail();
		$visita = (!$matrimonio->visita) ? new Visita : $matrimonio->visita;
		$tiposvisita = Tipovisita::all()->pluck('tipo' , 'id');
		return view('users.forms.form-visitas' , ['visita' => $visita , 'tiposvisita' => $tiposvisita]);
	}

	public function saveVisitasForm(Request $request)
	{
		$id = Auth::id();
		$matrimonio = Matrimonio::where('idUser', $id)->select('id' , 'cantidadHijos')->firstOrFail();
		$visita = $matrimonio->visita;
		if(!$matrimonio->hijos->isEmpty() || $matrimonio->cantidadHijos > 0)
		{
			if(!$visita)
			{
				$visita = new Visita;
			}
			$visita->fill($request->all());
			$visita->idMatrimonio = $matrimonio->id;
			$visita->save();
		}
		return redirect()->route('paso.final');
	}

	public function showPasoFinal()
	{
		return view('users.forms.form-paso-final');
	}

	public function savePasoFinal()
	{
		$user = Auth::user();
		$matrimonio = $user->matrimonio;
		$matrimonio->estado = 0;
		$matrimonio->save();
		$caso = Caso::firstOrCreate(['idMatrimonio' => $matrimonio->id]);

		//Generación de PDF de prototipo de demanda y guardado en carpeta archivos
		$data = $matrimonio;
		$filename = 'demanda-' . md5($user->email . rand(1,100));
		$pdf = PDF::loadView('pdf.demanda',compact('data'))->save('../archivos/'.$filename.'.pdf');

		//Envío prototipo de demanda al email
		Mail::send('mails.envio-demanda' , ['name' => 'Patricio'] , function(Message $message) use ($filename , $user){
			$message
			->to($user->email , $user->name)
			->attach('../archivos/'.$filename.'.pdf')
			->subject('Tu demanda');
		});

		//Elimina pdf demanda
		unlink('../archivos/'.$filename.'.pdf');

		return view('users.forms.mensaje-final');
	}

}
