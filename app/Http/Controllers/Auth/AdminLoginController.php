<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/admin';
    protected $maxAttempts = 3;
    protected $decayMinutes = 15;

	public function __construct()
	{
		$this->middleware('guest:admin')->except('logout');
	}
    public function showLoginForm()
    {
    	return view('admin.login');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

}
