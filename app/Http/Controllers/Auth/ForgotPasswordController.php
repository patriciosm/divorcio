<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $validation = $this->validator($request->all());
        if($validation->fails())
        {
            return response()->json(['errors' => $validation->errors()]);   
        }

        $response = $this->broker()->sendResetLink($request->only('email'));

        if($request->ajax())
        {
            if($response == Password::RESET_LINK_SENT)
            {
                return response()->json(['success' => Lang::get('passwords.sent')]);
            }
            else
            {
                return response()->json(['errors' => ['email' => Lang::get('passwords.user')] ]);
            }
        }

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($response)
                    : $this->sendResetLinkFailedResponse($request, $response);
    }

    protected function validator(array $data)
    {
        return Validator::make($data , [
            'email' =>'required|string'
        ]);
    }

}
