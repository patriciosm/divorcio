<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Caso;
use App\Models\Abogado;
use App\Models\Estado;
use App\Models\Formapago;

class CasoController extends Controller
{
	//Muestra el listado de casos
    public function listarCasos()
    {
        $casos = Caso::all();
        return view('admin.casos.lista-casos')->with('casos' , $casos);
    }

    //Muestra el caso seleccionado
    public function showCaso($id)
    {
        $caso = Caso::findOrFail($id);
        return view('admin.casos.ver-caso')->with('caso' , $caso);
    }

    //Muestra formulario de edición del caso seleccionado
    public function editCaso($id)
    {
        $caso = Caso::findOrFail($id);
        $abogados = Abogado::selectRaw('CONCAT (nombre, " " , apellido) AS nombreApellido , id')->pluck('nombreApellido','id');
        $estados = Estado::pluck('estado','id');
        $formapagos = Formapago::pluck('formapago','id');
        return view('admin.casos.formulario-casos', ['caso' => $caso , 'estados' => $estados , 'abogados' => $abogados , 'formapagos' => $formapagos]);
    }

    //Guarda las actualizaciones del caso en la BBDD
    public function updateCaso(Request $request)
    {
        $caso = Caso::findOrFail($request->input('id'));
        $caso->update($request->all());
        return redirect()->route('ver-caso',['id' => $request->input('id') ]);
    }

}
