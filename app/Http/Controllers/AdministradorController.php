<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdministradorController extends Controller
{
    
	//Muestra vista de inicio del panel de administración
    public function index()
    {
    	return view('admin.home-administracion');
    }
}
