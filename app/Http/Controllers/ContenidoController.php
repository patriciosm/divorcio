<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contenido;
use App\Models\Tarifa;

class ContenidoController extends Controller
{
    //Muestra listado de contenidos del Home
    public function showContenidos()
    {
        $contenidos = Contenido::all();
        $tarifas = Tarifa::all();
    	return view('admin.contenidos.lista-contenidos')->with('contenidos' , $contenidos)->with('tarifas' , $tarifas);
    }

    //Muestra formulario de edición de contenidos del Home
    public function editContenidos($id)
    {
        $contenidos = Contenido::findOrFail($id);
        return view('admin.contenidos.formulario-contenidos')->with('contenidos' , $contenidos); 
    }

    //Guarda las actualizaciones de contenidos del Home en la BBDD
    public function updateContenidos(Request $request)
    {
        $contenido = Contenido::findOrFail( $request->input('id') );
        $contenido->update($request->all());
        return redirect()->route('lista-contenidos');
    }

    //Muestra formulario para editar los datos de la tarifa
    public function editTarifa($id)
    {
        $tarifa = Tarifa::findOrFail($id);
        return view('admin.contenidos.formulario-tarifas')->with('tarifa' , $tarifa);
    }

    //Actualiza los datos de tarifa en BBDD
    public function updateTarifa(Request $request)
    {
        $tarifa = Tarifa::findOrFail($request->input('id'));
        $tarifa->update($request->all());
        return redirect()->route('lista-contenidos');
    }

    //Muestra formulario para el posteo de video en el Home
    /*public function createVideo()
    {
        return "Formulario de creación de video";
    }

    //Guarda en la BBDD los datos del video
    public function saveVideo()
    {
        

        //return redirect('admin/contenidos');
    }

    //Muestra formulario para actualización de datos de video
    public function editVideo($id)
    {
        return "Formulario de edición de video";
    }

    //Guarda en la BBDD los datos del video
    public function updateVideo($id)
    {
        

        //return redirect('admin/contenidos');
    }

    //Confirmación de eliminación de video
    public function getDeleteVideo($id)
    {
        return "Confirmación de eliminación de video";
    }

    //Elimina video de BBDD
    public function deleteVideo()
    {
        //return redirect('admin/contenidos');
    }*/
}
