<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contenido;
use App\Models\Tarifa;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $contenidos = Contenido::all();
        $tarifas = Tarifa::all();
        return view('home' , ['contenidos' => $contenidos , 'tarifas' => $tarifas]);
    }

}
