<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comuna;

class ComunasController extends Controller
{
    public function listarComunas($idRegion="0")
    {
    	$comunas = Comuna::select('id' , 'idRegion' , 'comuna')->where('idRegion' , $idRegion)->orderBy('comuna')->get();
    	return json_encode($comunas);
    }
}
