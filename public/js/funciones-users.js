﻿$(document).ready(function()
{
	var menuFormulario = (function()
	{
		var $menuForm= $('.menu-formulario ul');
		var is_open = 0;
		var $botonMenuForm = $('.menu-formulario .btn-chico');
		var texto = $botonMenuForm.html();
		
		$botonMenuForm.html(texto + ' +')
		$menuForm.addClass('menu-formulario-inactivo');
		
		$botonMenuForm.click(function(event)
		{
			event.preventDefault();
			$menuForm.toggleClass('menu-formulario-activo');
			if(is_open == 0)
			{
				$(this).html(texto + ' -');
				is_open = 1;
				
			}
			else{
				$(this).html(texto + ' +');
				is_open = 0;
			}
		});
	})();

	var obtenerComunas = (function()
	{
		var $region = $('.region');
		var $comuna = $('.comuna');
		$region.change(function()
		{
			var $indice = $(this).index('.region');
			var $listaComunas = $comuna.eq($indice);
			$listaComunas.empty();
			var html = "<option value selected='selected'>Seleccione comuna</option>";
			var idRegion = $(this).val();
			if( idRegion != 0 )
			{
				$.getJSON("/formulario/comuna/" + idRegion).
				done(function(data)
				{
					$.each(data , function(opcionesIndex , opciones)
					{
						html += "<option value='" + opciones['id'] + "'>" + opciones['comuna'] + "</option>";
					});
					$listaComunas.append(html);
				}).
				fail(function()
				{
					console.log("Error");
				});
			}
			else
			{
				$listaComunas.append(html);
			}
		});
	})();

	var creaCampos = (function()
	{
		var $contenedores = $('.contenedor-campos');
		var indice = $contenedores.length;
		var $contenedorNuevo = $('#contenedor_nuevos_campos');
		var $agregaBtn = $('#agrega');
		var $formulario = $contenedores.eq(indice - 1);
		
		$agregaBtn.click(function(event){
			event.preventDefault();
			var $copiaForm = $formulario.clone();
			indice++;
			$copiaForm.find("[name^='exp']").each(function()
			{
				var expresion = /exp\[\d+\]/;
				var atributo = $(this).attr('name').replace(expresion , 'exp['+ indice +']');
				$(this).attr('name' , atributo).val('');
			});
			$contenedorNuevo.append($copiaForm);
		});
	})();

});

// (function($){
// 	$.fn.extend({
// 		mostrardatos: function(options){
// 			defaults = {
// 				opcionElegida: '' 
// 			}
// 			var options = $.extend({} , defaults , options);
			
// 			var $mostrarBloque = $('.mostrar-bloque');
// 			var cantBloques = $mostrarBloque.length;
// 			var $radios = $(this);
// 			var totalOpciones = $radios.length;
// 			var opcionesBloque = totalOpciones/cantBloques;
			
// 			for(i = 0 ; i < totalOpciones ; i = i + opcionesBloque)
// 			{
// 				var bloque = Math.floor(i/opcionesBloque);
// 				valor = $radios.slice(i,opcionesBloque*(i+1)).filter(':checked').val();
// 				if(valor == null || valor != options.opcionElegida)
// 				{
// 					$mostrarBloque.eq(bloque).css({display:'none'});
// 				}
// 			}
			
// 			$radios.each(function(index){
// 				$(this).change(function()
// 				{
// 					$actual = $(this);
// 					var bloque = Math.floor(index/opcionesBloque);
// 					if($actual.val() == options.opcionElegida)
// 					{
// 						$mostrarBloque.eq(bloque).slideDown();
// 					}
// 					else
// 					{
// 						$mostrarBloque.eq(bloque).slideUp();
// 					}
// 				});
// 			});
// 		}
// 	});
// })(jQuery)