$(document).ready(function()
{

	var menu = (function()
	{
		var is_open = 0;
		var $menu = $('nav');
		var $ventana = $(window);
		var $cuerpo = $('.cuerpo');
		var altoCabecera = $cuerpo.offset().top;

		$ventana.resize(redimensionaVentana);

		function redimensionaVentana()
		{
			var anchoVentana = window.innerWidth;
			if( anchoVentana < 800 )
			{
				$menu.css({'right':'-100%'});
			}
			else
			{
				$menu.css({'right':'0'});
			}
			is_open = 0;
		}

		$('.boton-menu').click(menu_responsive);

		function menu_responsive(event)
		{
			event.preventDefault();
			if( is_open == 1 )
			{
				close_nav();
			}
			else
			{
				open_nav();
			}
		}
		
		var open_nav = function()
		{
			$menu.animate({'right':'0'});
			is_open = 1;
		}
		
		var close_nav = function()
		{
			$menu.animate({'right':'-100%'});
			is_open = 0;
		}

		var $btnArriba = $("<a href='#arriba' class='btn-up'><span class='icon-up'>Arriba</span></a>");
		$('body').append($btnArriba);
		$btnArriba.css({'position':'fixed' , 'bottom':'30px' , 'right':'20px'});

		$('nav ul a').click(deslizar);
		$btnArriba.click(deslizar);

		$ventana.scroll(function()
		{
			var posicionVentana = $(this).scrollTop();
			if( posicionVentana > altoCabecera )
			{
				$btnArriba.css('visibility','visible');
			}
			else
			{
				$btnArriba.css('visibility','hidden');
			}
		});

		function deslizar(event)
		{
			event.preventDefault();
			var $destino = $(this).attr('href');
			var indice = $destino.search('#');
			$destino = $destino.substr(indice);
			var posicion = $($destino).offset().top;
			var anchoVentana = window.innerWidth;
			var $body = $('body , html');
			if(anchoVentana < 800)
			{
				close_nav();
			}
			$body.animate({scrollTop:posicion},1000);
		}

	})();
	
	var slider = (function(){
		var totalSlides = $('.slide').length;
		var $listaSlides = $('.lista-slider');
		var actualSlide = 0;
		var proximaSlide = 1;
		var sliderInterval;
		
		$("<div class='paginacion-slider'></div>").appendTo('.contenedor-slider');
		for( i=1;i <= totalSlides;i++ )
		{
			$("<span class='item-paginacion'></span>").appendTo('.paginacion-slider');
		}
		
		var $itemPaginacion = $('.item-paginacion');
		$itemPaginacion.eq(0).addClass("item-paginacion-activo");
		
		var movSlider = function(){
			if( proximaSlide >= totalSlides )
			{
				proximaSlide = 0;
			}
			
			$itemPaginacion.removeClass("item-paginacion-activo").eq(proximaSlide).addClass("item-paginacion-activo");
			$listaSlides.animate({'left':(-100 * proximaSlide)+'%'},1000);
			
			actualSlide = proximaSlide;
			proximaSlide += 1;
		}
		
		var sliderInit = function(){
			sliderInterval = setInterval(movSlider,6000);
		}
		sliderInit();
		
		$itemPaginacion.click(function(){
			var slideDestino = $(this).index();
			if( slideDestino != actualSlide ){
				proximaSlide = slideDestino;
				movSlider();
				clearInterval(sliderInterval);
				sliderInit();
			}
		});

	})();

	var animacion_cajas = (function()
	{
		var $servicio = $('.servicio');
		var contador = 0;
		var cantServicios = $servicio.length;
		
		var activaServicio = function(){
			if( contador >= cantServicios )
			{
				contador = 0;
			}	
			$servicio.removeClass('servicio-activo');
			$servicio.eq(contador).addClass('servicio-activo');
			contador += 1;
		}
		var servicioInit = function(){
			servicioInterval = setInterval(activaServicio,4000);
		}
		activaServicio();
		servicioInit();
	})();

	var modal = (function()
	{
		$sesion_modal = $('.modal-sesion').clone();
		$registro_modal = $('.modal-registro').clone();
		$olvido_modal = $('.modal-olvido').clone();
		$modal_wrapper = $('.modal-wrapper');

		var is_open = 0;

		function open_modal()
		{
			$modal_wrapper.children('div').remove();
			if(is_open == 0){
				$('.modal-fondo').css('display','flex');
				$('body').addClass('open-modal');
				is_open = 1;
			}
		}

		function cierre_modal()
		{
			$('.modal-fondo').css('display','none');
			$('body').removeClass('open-modal');
			$modal_wrapper.find('form')[0].reset();
			$modal_wrapper.children('div').remove();
			is_open = 0;
		}

		$('.modal-close').click(function(e)
		{
			e.preventDefault();
			cierre_modal();
		});
		
		$('.modal-contenedor').click(function(e)
		{
			if(e.target === this)
			{
				cierre_modal();
			}
		});

		function carga_modal(e , $modal)
		{
			e.preventDefault();
			open_modal();
			$modal.appendTo($modal_wrapper);
			var $form = $modal_wrapper.find('form');
			$form[0].reset();
			$form.find('.mensaje-error').html('');
			$olvido_modal.find('.mensaje-exito').html('');
			$olvido_modal.find('form , .submit-olvido').show();
		}

		$('.btn-sesion').click(function() { carga_modal(event , $sesion_modal); } );

		$('.btn-registro').click(function() { carga_modal(event , $registro_modal); } );

		$modal_wrapper.on('click','a.enlace',function()
		{
			var enlace = $(this).attr('data-enlace');

			switch(enlace)
			{
				case 'btn-sesion': carga_modal(event , $sesion_modal);
					break;
				case 'btn-registro': carga_modal(event , $registro_modal);
					break;
				default: carga_modal(event , $olvido_modal);
			}
		});
		

		$modal_wrapper.on('click' , '.submit-sesion', function(e){
			e.preventDefault();
			$boton = $(this).hide();
			var $formulario = $(this).parents('form');
			var datos = $formulario.serialize();
			var ruta = $formulario.attr('action');
			$.ajax({
				type: "POST",
				url: ruta,
				cache: false,
				data: datos,
				dataType: 'json'
			})
			.done(function(data)
			{
				if(data.errors)
				{
					$boton.show();
					$formulario.find('.error-datos').html(data.errors);
				}
				else if (data.errores)
				{
					$boton.show();
					$formulario.find('.email-error').html(data.errores.email);
					$formulario.find('.password-error').html(data.errores.password);
				}
				else
				{
					location.href="/dashboard";
				}
			})
			.fail(function(data)
			{
				console.log('Problemas con la conexión');
			});
		});

		$modal_wrapper.on('click' , '.submit-registro', function(e){
			e.preventDefault();
			$boton = $(this).hide();
			var $formulario = $(this).parents('form');
			var datos = $formulario.serialize();
			var ruta = $formulario.attr('action');
			$.ajax({
				type: "POST",
				url: ruta,
				cache: false,
				data: datos,
				dataType: 'json'
			})
			.done(function(data)
			{
				if(data.errors)
				{
					$boton.show();
					$formulario.find('.nombre-error').html(data.errors.nombre);
					$formulario.find('.email-error').html(data.errors.email);
					$formulario.find('.password-error').html(data.errors.password);
				}
				if(data.success)
				{
					window.location.replace('/dashboard');
				}
			})
			.fail(function(data)
			{
				console.log('Problemas con la conexión');
			});
		});

		$modal_wrapper.on('click' , '.submit-olvido', function(e){
			e.preventDefault();
			$boton = $(this).hide();
			var $formulario = $(this).parents('form');
			var datos = $formulario.serialize();
			var ruta = $formulario.attr('action');
			$.ajax({
				type: "POST",
				url: ruta,
				cache: false,
				data: datos,
				dataType: 'json'
			})
			.done(function(data)
			{
				if(data.errors)
				{
					$boton.show();
					$formulario.find('.email-error').html(data.errors.email);
				}
				else
				{
					$formulario.hide();
					$formulario.next('.mensaje-exito').html(data.success);
				}
			})
			.fail(function(data)
			{
				console.log('Problemas con la conexión');
			});
		});

	})();

});
