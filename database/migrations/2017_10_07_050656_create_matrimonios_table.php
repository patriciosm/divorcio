<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatrimoniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matrimonios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUser')->unsigned();
            $table->date('fechaMatrimonio')->nullable();
            $table->date('ceseConvivencia')->nullable();
            $table->string('dejaHogar',6)->nullable();
            $table->boolean('renunciaCompensacion')->nullable();
            $table->string('favorCompensacion',6)->nullable();
            $table->integer('montoCompensacion')->unsigned()->default(0);
            $table->boolean('tieneBienes')->nullable();
            $table->string('nombresMarido',30)->nullable();
            $table->string('apellidosMarido',30)->nullable();
            $table->string('rutMarido',12)->nullable();
            $table->string('ocupacionMarido',20)->nullable();
            $table->string('domicilioMarido',100)->nullable();
            $table->smallInteger('idComunaMarido')->unsigned()->nullable();
            $table->string('emailMarido',50)->nullable();
            $table->string('telefonoMarido',9)->nullable();
            $table->string('nombresMujer',30)->nullable();
            $table->string('apellidosMujer',30)->nullable();
            $table->string('rutMujer',12)->nullable();
            $table->string('ocupacionMujer',20)->nullable();
            $table->string('domicilioMujer',100)->nullable();
            $table->smallInteger('idComunaMujer')->unsigned()->nullable();
            $table->string('emailMujer',50)->nullable();
            $table->string('telefonoMujer',9)->nullable();
            $table->tinyInteger('cantidadHijos')->unsigned()->default(0);
            $table->integer('montoPension')->unsigned()->default(0);
            $table->boolean('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matrimonios');
    }
}
