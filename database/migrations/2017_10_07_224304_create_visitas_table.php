<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idMatrimonio')->unsigned()->unique();
            $table->tinyInteger('idTipovisita')->unsigned()->nullable();
            $table->string('otraVisita' , 100)->nullable();
            $table->time('horaRetiro')->nullable();
            $table->time('horaRestitucion')->nullable();
            $table->date('fechaInicioVerano')->nullable();
            $table->date('fechaFinVerano')->nullable();
            $table->string('otroVerano')->nullable();
            $table->tinyInteger('diasInvierno')->unsigned()->nullable();
            $table->string('cumpleanos',5)->nullable();
            $table->string('fiestasPatrias',5)->nullable();
            $table->boolean('diaFestejado')->default(0);
            $table->string('cumplePadres',5)->nullable();
            $table->boolean('diaPadreMadre')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitas');
    }
}
