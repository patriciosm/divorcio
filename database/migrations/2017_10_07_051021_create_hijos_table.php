<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHijosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hijos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idMatrimonio')->unsigned();
            $table->string('nombres',30)->nullable();
            $table->string('apellidos',30)->nullable();
            $table->tinyInteger('edad')->unsigned()->nullable();
            $table->string('rut',10)->nullable();
            $table->tinyInteger('idOcupacion')->unsigned()->nullable();
            $table->tinyInteger('idTuicion')->unsigned()->nullable();
            $table->string('lugar',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hijos');
    }
}
