<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
		$this->call(AbogadosTableSeeder::class);
		$this->call(ContenidosTableSeeder::class);
		$this->call(EstadosTableSeeder::class);
		$this->call(SeccionesTableSeeder::class);       
		$this->call(TarifasTableSeeder::class);
		$this->call(TiposdatosTableSeeder::class);
        $this->call(RegionesTableSeeder::class);
        $this->call(ComunasTableSeeder::class);
        $this->call(TuicionesTableSeeder::class);
        $this->call(TiposvisitasTableSeeder::class);
        $this->call(OcupacionesTableSeeder::class);
        $this->call(FormaspagosTableSeeder::class);
        $this->call(MatrimoniosTableSeeder::class);
        $this->call(HorastomadasTableSeeder::class);
        $this->call(HorasaudienciasTableSeeder::class);
        $this->call(PagosTableSeeder::class);
        $this->call(CasosTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(BienesTableSeeder::class);
        $this->call(HijosTableSeeder::class);
        $this->call(VisitasTableSeeder::class);
        $this->call(TestigosTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
