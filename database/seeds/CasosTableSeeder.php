<?php

use Illuminate\Database\Seeder;

class CasosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('casos')->insert(array(
                        array(
                        	'idMatrimonio' => 1 ,
                        	'idEstado' => 2 ,
                        	'idAbogado' => 2 ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        	) 
        ));
    }
}
