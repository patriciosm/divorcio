<?php

use Illuminate\Database\Seeder;

class SeccionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('secciones')->insert(array(
                        array('seccion' => 'Slider') ,
                        array('seccion' => 'Funciona') ,
                        array('seccion' => 'Servicio') ,
                        array('seccion' => 'Conoce') ,
                        array('seccion' => 'Blog') 
        ));
    }
}
