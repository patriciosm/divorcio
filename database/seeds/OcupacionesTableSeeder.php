<?php

use Illuminate\Database\Seeder;

class OcupacionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ocupaciones')->insert(array(
                        array('ocupacion' => 'Estudia') ,
                        array('ocupacion' => 'Trabaja') ,
                        array('ocupacion' => 'Estudia y trabaja') 
        ));
    }
}
