<?php

use Illuminate\Database\Seeder;

class ContenidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contenidos')->insert(array(

                        array(
                        	'idSeccion' => 1 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => '¿Necesitas divorciarte de mutuo acuerdo?' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Pero no tienes el tiempo suficiente para realizar el tramite y necesitas un servicio que incluya todos los costos.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ) ,

                        array(
                        	'idSeccion' => 1 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Tramitación completamente en linea' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Con nuestro formulario podras acordar las clausulas del divorcio.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ) ,

                        array(
                        	'idSeccion' => 1 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'El servicio incluye todos los costos' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Cras eget ante at neque tristique feugiat at nec metus. Ut sit amet sollicitudin tellus. In ac laoreet lacus.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ) ,

                        array(
                        	'idSeccion' => 2 ,
                        	'idTipodato' => 1 ,
                        	'titulo' => '¿Cómo funciona?' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p class="fuente-alternativa texto-naranjo">En divorcio en línea, tramitamos tu Divorcio de Mutuo Acuerdo, según lo expuesto en la ley 19.947.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 2 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Regístrate' ,
                        	'subtitulo' => 'icon-registrate' ,
                        	'texto' => '<p>Crea tu cuenta ingresando tus datos personales.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 2 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Formulario' ,
                        	'subtitulo' => 'icon-formulario' ,
                        	'texto' => '<p>Llena el formulario en línea de manera gratuita.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 2 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Firma' ,
                        	'subtitulo' => 'icon-firma' ,
                        	'texto' => '<p>Firma la demanda confeccionada por nuestros abogados.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 2 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Cuenta' ,
                        	'subtitulo' => 'icon-cuenta' ,
                        	'texto' => '<p>A través de tu cuenta, sigue todo el procedimiento en línea.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 2 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Audiencia' ,
                        	'subtitulo' => 'icon-audiencia' ,
                        	'texto' => '<p>Presentate a la audiencia en compañía de nuestros abogados.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 2 ,
                        	'idTipodato' => 2 ,
                        	'titulo' => '¡Y listo!' ,
                        	'subtitulo' => 'ya se encuentra divorciado' ,
                        	'texto' => '<p class="fuente-alternativa">Ahora inscribirémos la sentencia de divorcio en el Registro Civil de Identificación.</p>
<p>Una vez todo listo solicite el envío de toda la documentación de su divorcio, de manera gratuita, mediante correo certificado en el sitio donde usted nos indique, o mediante retiro en nuestras oficinas.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 3 ,
                        	'idTipodato' => 1 ,
                        	'titulo' => 'Nuestro servicio' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p class="fuente-alternativa">En divorcio en línea obtendrás:</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 3 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Título 1' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Completa tramitación de un Divorcio de Mutuo Acuerdo mediante ley 19.947.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 3 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => '2 abogados' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Vestibulum ultrices quam nisi, at dictum enim mattis eu.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 3 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Reuniones' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Phasellus dignissim facilisis consectetur. Mauris facilisis nulla ac dapibus convallis.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 3 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Inscripción' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Nullam maximus, quam nec accumsan lobortis, sem odio venenatis dui, sed ultrices sapien nisi ac nisi.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 3 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Costos' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Suspendisse velit ipsum, euismod in elit vitae, vehicula tempus urna.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 4 ,
                        	'idTipodato' => 1 ,
                        	'titulo' => 'Conoce más' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p class="fuente-alternativa texto-naranjo">En Divorcio en línea, nos gusta que estes informado, conoce todos los detalles de nuestro servicio.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 4 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Requisitos' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Solo para matrimonios celebrados hasta antes del día 17 de noviembre del 2004.<br>
                                    (Fecha de celebración posterior solicitar reunión previa)</p>
                                    <p>Cese de convivencia entre los conyuges de a lo menos un año</p>
                                    <p>3 testigos que asistan el día de la audiencia y que declaren conocer a los cónyuges y tengan conocimiento que no han vuelto a convivir juntos nuevamente (pueden ser familiares, amigos o conocidos).</p>' ,
                                    'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 4 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Beneficios' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non sapien vehicula, pulvinar ligula sed, faucibus nulla. Aenean sapien massa, molestie in urna non, eleifend suscipit massa. Donec eu magna lacinia, vulputate diam ac, tempus orci. Sed ullamcorper quam felis, ut ullamcorper nunc tempus quis.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 4 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Tramitación' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Curabitur sed nunc magna. Nunc feugiat accumsan condimentum. Cras semper rhoncus pellentesque. Suspendisse fermentum elit varius elit efficitur molestie. Fusce purus dui, lobortis finibus dui non, bibendum ultrices ex.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 4 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Tarifas' ,
                        	'subtitulo' => '' ,
                        	'texto' => '' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 4 ,
                        	'idTipodato' => 3 ,
                        	'titulo' => 'Importante' ,
                        	'subtitulo' => '' ,
                        	'texto' => '<p>Mauris semper nibh pharetra purus vulputate, nec gravida urna sodales. Donec vestibulum lectus scelerisque lectus volutpat feugiat. Donec arcu mauris, blandit ut dolor sed, tincidunt rutrum sem. Vestibulum congue tincidunt rutrum. Sed ut venenatis metus. Cras vitae dolor ipsum.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        ),

                        array(
                        	'idSeccion' => 5 ,
                        	'idTipodato' => 1 ,
                        	'titulo' => 'Nuestro Blog' ,
                        	'subtitulo' => '¿Tienes otras dudas legales?' ,
                        	'texto' => '<p class="fuente-alternativa">Obtén respuestas gratuitas en línea.</p>
                              <p>Se parte de la comunidad legal y asesorate gratuitamente con nuestros abogados en línea.</p>' ,
                              'created_at' => date("Y-m-d H:i:s") ,
                              'updated_at' => date("Y-m-d H:i:s")
                        )

        ));
    }
}
