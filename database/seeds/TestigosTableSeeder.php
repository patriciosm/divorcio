<?php

use Illuminate\Database\Seeder;

class TestigosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('testigos')->insert(array(
                        array(
                        	'idCaso' => 1 ,
                        	'nombre' => 'Juan' ,
                        	'apellido' => 'Verdugo' ,
                        	'rut' => '12637846-K' ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        	) ,
                        array(
                        	'idCaso' => 2 ,
                        	'nombre' => 'Roberto' ,
                        	'apellido' => 'Cisternas' ,
                        	'rut' => '11008373-2' ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        	) ,
                        array(
                        	'idCaso' => 1 ,
                        	'nombre' => 'Alvaro' ,
                        	'apellido' => 'Poblete' ,
                        	'rut' => '8242763-4' ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        	)
        ));
    }
}
