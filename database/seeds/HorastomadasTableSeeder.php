<?php

use Illuminate\Database\Seeder;

class HorastomadasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('horastomadas')->insert(array(
                        array(
                            'idCaso' => 1,
                        	'fecha' => '2017-10-12' ,
                        	'hora' => '10:30:00' ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        ) ,
                        array(
                            'idCaso' => 1,
                        	'fecha' => '2017-10-16' ,
                        	'hora' => '11:00:00' ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        )
        ));
    }
}
