<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
                        array(
                        	'name' => 'patricio',
                        	'email' => 'patriciosm@gmail.com',
                        	'password' => bcrypt('p4ntr023')
                        ) , 
                        array(
                            'name' => 'simon',
                            'email' => 'simon@gmail.com',
                            'password' => bcrypt('simon')
                        ) ,
                        array(
                            'name' => 'silvia',
                            'email' => 'silvia@gmail.com',
                            'password' => bcrypt('silvia')
                        )
        ));
    }
}
