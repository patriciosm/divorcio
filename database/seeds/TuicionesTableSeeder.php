<?php

use Illuminate\Database\Seeder;

class TuicionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tuiciones')->insert(array(
                        array('tutor' => 'Madre') ,
                        array('tutor' => 'Padre') ,
                        array('tutor' => 'Otro') 
        ));
    }
}
