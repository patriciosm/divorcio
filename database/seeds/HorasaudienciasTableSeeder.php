<?php

use Illuminate\Database\Seeder;

class HorasaudienciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('horasaudiencias')->insert(array(
                        array(
                            'idCaso' => 1,
                        	'fecha' => '2017-10-17' ,
                        	'hora' => '12:00:00' ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        )
        ));
    }
}
