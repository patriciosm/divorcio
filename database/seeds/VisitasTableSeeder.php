<?php

use Illuminate\Database\Seeder;

class VisitasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('visitas')->insert(array(
                        array(
                        	'idMatrimonio' => 1 ,
                        	'idTipovisita' => 2 ,
                            'otraVisita' => '' ,
                        	'horaRetiro' => '10:00:00' ,
                        	'horaRestitucion' => '19:00:00' ,
                        	'fechaInicioVerano' => '2017-01-05' ,
                        	'fechaFinVerano' => '2017-01-15' ,
                        	'otroVerano' => '' ,
                        	'diasInvierno' => 5 ,
                        	'cumpleanos' => 'Padre' ,
                        	'fiestasPatrias' => 'Madre' ,
                        	'diaFestejado' => 1 ,
                        	'cumplePadres' => 'Padre' ,
                        	'diaPadreMadre' => 1 ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        	) 
        ));
    }
}
