<?php

use Illuminate\Database\Seeder;

class PagosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pagos')->insert(array(
                        array(
                        	'idCaso' => 1 ,
                        	'valor' => 50000 ,
                        	'fecha' => '2017-08-10' ,
                        	'idFormapago' => 1 ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        ) ,
                        array(
                        	'idCaso' => 1 ,
                        	'valor' => 25000 ,
                        	'fecha' => '2017-10-03' ,
                        	'idFormapago' => 2 ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        ) ,
                        array(
                        	'idCaso' => 2 ,
                        	'valor' => 40000 ,
                        	'fecha' => '2017-10-17' ,
                        	'idFormapago' => 2 ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        ) ,
                        array(
                        	'idCaso' => 1 ,
                        	'valor' => 15000 ,
                        	'fecha' => '2017-10-17' ,
                        	'idFormapago' => 2 ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        )
        ));
    }
}
