<?php

use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados')->insert(array(
                        array('estado' => 'Revisión de datos') ,
                        array('estado' => 'Presentación tribunal') ,
                        array('estado' => 'Terminado') 
        ));
    }
}
