<?php

use Illuminate\Database\Seeder;

class ComunasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comunas')->insert(array(
			array(
				'idRegion' => 15 ,
				'comuna' => 'Arica'
			) , array(
				'idRegion' => 15 ,
				'comuna' => 'Camarones'
			) , array(
				'idRegion' => 15 ,
				'comuna' => 'Putre'
			) , array(
				'idRegion' => 15 ,
				'comuna' => 'General Lagos'
			) , array(
				'idRegion' => 1 ,
				'comuna' => 'Iquique'
			) , array(
				'idRegion' => 1 ,
				'comuna' => 'Alto Hospicio'
			) , array(
				'idRegion' => 1 ,
				'comuna' => 'Pozo Almonte'
			) , array(
				'idRegion' => 1 ,
				'comuna' => 'Camiña'
			) , array(
				'idRegion' => 1 ,
				'comuna' => 'Colchane'
			) , array(
				'idRegion' => 1 ,
				'comuna' => 'Huara'
			) , array(
				'idRegion' => 1 ,
				'comuna' => 'Pica'
			) , array(
				'idRegion' => 2 ,
				'comuna' => 'Antofagasta'
			) , array(
				'idRegion' => 2 ,
				'comuna' => 'Mejillones'
			) , array(
				'idRegion' => 2 ,
				'comuna' => 'Sierra Gorda'
			) , array(
				'idRegion' => 2 ,
				'comuna' => 'Taltal'
			) , array(
				'idRegion' => 2 ,
				'comuna' => 'Calama'
			) , array(
				'idRegion' => 2 ,
				'comuna' => 'Ollagüe'
			) , array(
				'idRegion' => 2 ,
				'comuna' => 'San Pedro de Atacama'
			) , array(
				'idRegion' => 2 ,
				'comuna' => 'Tocopilla'
			) , array(
				'idRegion' => 2 ,
				'comuna' => 'María Elena'
			) , array(
				'idRegion' => 3 ,
				'comuna' => 'Copiapó'
			) , array(
				'idRegion' => 3 ,
				'comuna' => 'Caldera'
			) , array(
				'idRegion' => 3 ,
				'comuna' => 'Tierra Amarilla'
			) , array(
				'idRegion' => 3 ,
				'comuna' => 'Chañaral'
			) , array(
				'idRegion' => 3 ,
				'comuna' => 'Diego de Almagro'
			) , array(
				'idRegion' => 3 ,
				'comuna' => 'Vallenar'
			) , array(
				'idRegion' => 3 ,
				'comuna' => 'Alto del Carmen'
			) , array(
				'idRegion' => 3 ,
				'comuna' => 'Freirina'
			) , array(
				'idRegion' => 3 ,
				'comuna' => 'Huasco'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'La Serena'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Coquimbo'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Andacollo'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'La Higuera'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Paiguano'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Vicuña'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Illapel'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Canela'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Los Vilos'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Salamanca'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Ovalle'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Combarbalá'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Monte Patria'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Punitaqui'
			) , array(
				'idRegion' => 4 ,
				'comuna' => 'Río Hurtado'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Valparaíso'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Casablanca'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Concón'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Juan Fernández'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Puchuncaví'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Quintero'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Viña del Mar'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Isla de Pascua'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Los Andes'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Calle Larga'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Rinconada'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'San Esteban'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'La Ligua'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Cabildo'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Papudo'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Petorca'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Zapallar'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Quillota'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Calera'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Hijuelas'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'La Cruz'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Nogales'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'San Antonio'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Algarrobo'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Cartagena'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'El Quisco'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'El Tabo'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Santo Domingo'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'San Felipe'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Catemu'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Llaillay'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Panquehue'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Putaendo'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Santa María'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Quilpué'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Limache'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Olmué'
			) , array(
				'idRegion' => 5 ,
				'comuna' => 'Villa Alemana'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Rancagua'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Codegua'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Coinco'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Coltauco'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Doñihue'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Graneros'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Las Cabras'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Machalí'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Malloa'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Mostazal'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Olivar'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Peumo'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Pichidegua'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Quinta de Tilcoco'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Rengo'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Requínoa'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'San Vicente'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Pichilemu'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'La Estrella'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Litueche'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Marchihue'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Navidad'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Paredones'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'San Fernando'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Chépica'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Chimbarongo'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Lolol'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Nancagua'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Palmilla'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Peralillo'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Placilla'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Pumanque'
			) , array(
				'idRegion' => 6 ,
				'comuna' => 'Santa Cruz'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Talca'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Constitución'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Curepto'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Empedrado'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Maule'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Pelarco'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Pencahue'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Río Claro'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'San Clemente'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'San Rafael'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Cauquenes'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Chanco'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Pelluhue'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Curicó'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Hualañé'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Licantén'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Molina'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Rauco'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Romeral'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Sagrada Familia'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Teno'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Vichuquén'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Linares'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Colbún'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Longaví'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Parral'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Retiro'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'San Javier'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Villa Alegre'
			) , array(
				'idRegion' => 7 ,
				'comuna' => 'Yerbas Buenas'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Concepción'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Coronel'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Chiguayante'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Florida'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Hualqui'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Lota'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Penco'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'San Pedro de la Paz'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Santa Juana'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Talcahuano'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Tomé'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Hualpén'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Lebu'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Arauco'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Cañete'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Contulmo'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Curanilahue'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Los Álamos'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Tirúa'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Los Ángeles'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Antuco'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Cabrero'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Laja'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Mulchén'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Nacimiento'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Negrete'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Quilaco'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Quilleco'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'San Rosendo'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Santa Bárbara'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Tucapel'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Yumbel'
			) , array(
				'idRegion' => 8 ,
				'comuna' => 'Alto Biobío'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Chillán'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Bulnes'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Cobquecura'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Coelemu'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Coihueco'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Chillán Viejo'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'El Carmen'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Ninhue'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Ñiquén'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Pemuco'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Pinto'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Portezuelo'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Quillón'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Quirihue'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Ránquil'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'San Carlos'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'San Fabián'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'San Ignacio'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'San Nicolás'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Treguaco'
			) , array(
				'idRegion' => 16 ,
				'comuna' => 'Yungay'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Temuco'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Carahue'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Cunco'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Curarrehue'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Freire'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Galvarino'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Gorbea'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Lautaro'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Loncoche'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Melipeuco'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Nueva Imperial'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Padre las Casas'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Perquenco'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Pitrufquén'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Pucón'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Saavedra'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Teodoro Schmidt'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Toltén'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Vilcún'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Villarrica'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Cholchol'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Angol'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Collipulli'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Curacautín'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Ercilla'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Lonquimay'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Los Sauces'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Lumaco'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Purén'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Renaico'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Traiguén'
			) , array(
				'idRegion' => 9 ,
				'comuna' => 'Victoria'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'Valdivia'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'Corral'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'Lanco'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'Los Lagos'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'Máfil'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'Mariquina'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'Paillaco'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'Panguipulli'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'La Unión'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'Futrono'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'Lago Ranco'
			) , array(
				'idRegion' => 14 ,
				'comuna' => 'Río Bueno'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Puerto Montt'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Calbuco'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Cochamó'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Fresia'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Frutillar'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Los Muermos'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Llanquihue'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Maullín'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Puerto Varas'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Castro'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Ancud'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Chonchi'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Curaco de Vélez'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Dalcahue'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Puqueldón'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Queilén'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Quellón'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Quemchi'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Quinchao'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Osorno'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Puerto Octay'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Purranque'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Puyehue'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Río Negro'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'San Juan de la Costa'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'San Pablo'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Chaitén'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Futaleufú'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Hualaihué'
			) , array(
				'idRegion' => 10 ,
				'comuna' => 'Palena'
			) , array(
				'idRegion' => 11 ,
				'comuna' => 'Coihaique'
			) , array(
				'idRegion' => 11 ,
				'comuna' => 'Lago Verde'
			) , array(
				'idRegion' => 11 ,
				'comuna' => 'Aisén'
			) , array(
				'idRegion' => 11 ,
				'comuna' => 'Cisnes'
			) , array(
				'idRegion' => 11 ,
				'comuna' => 'Guaitecas'
			) , array(
				'idRegion' => 11 ,
				'comuna' => 'Cochrane'
			) , array(
				'idRegion' => 11 ,
				'comuna' => 'O’Higgins'
			) , array(
				'idRegion' => 11 ,
				'comuna' => 'Tortel'
			) , array(
				'idRegion' => 11 ,
				'comuna' => 'Chile Chico'
			) , array(
				'idRegion' => 11 ,
				'comuna' => 'Río Ibáñez'
			) , array(
				'idRegion' => 12 ,
				'comuna' => 'Punta Arenas'
			) , array(
				'idRegion' => 12 ,
				'comuna' => 'Laguna Blanca'
			) , array(
				'idRegion' => 12 ,
				'comuna' => 'Río Verde'
			) , array(
				'idRegion' => 12 ,
				'comuna' => 'San Gregorio'
			) , array(
				'idRegion' => 12 ,
				'comuna' => 'Cabo de Hornos'
			) , array(
				'idRegion' => 12 ,
				'comuna' => 'Antártica'
			) , array(
				'idRegion' => 12 ,
				'comuna' => 'Porvenir'
			) , array(
				'idRegion' => 12 ,
				'comuna' => 'Primavera'
			) , array(
				'idRegion' => 12 ,
				'comuna' => 'Timaukel'
			) , array(
				'idRegion' => 12 ,
				'comuna' => 'Natales'
			) , array(
				'idRegion' => 12 ,
				'comuna' => 'Torres del Paine'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Santiago'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Cerrillos'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Cerro Navia'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Conchalí'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'El Bosque'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Estación Central'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Huechuraba'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Independencia'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'La Cisterna'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'La Florida'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'La Granja'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'La Pintana'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'La Reina'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Las Condes'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Lo Barnechea'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Lo Espejo'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Lo Prado'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Macul'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Maipú'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Ñuñoa'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Pedro Aguirre Cerda'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Peñalolén'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Providencia'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Pudahuel'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Quilicura'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Quinta Normal'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Recoleta'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Renca'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'San Joaquín'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'San Miguel'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'San Ramón'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Vitacura'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Puente Alto'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Pirque'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'San José de Maipo'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Colina'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Lampa'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Tiltil'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'San Bernardo'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Buin'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Calera de Tango'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Paine'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Melipilla'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Alhué'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Curacaví'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'María Pinto'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'San Pedro'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Talagante'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'El Monte'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Isla de Maipo'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Padre Hurtado'
			) , array(
				'idRegion' => 13 ,
				'comuna' => 'Peñaflor'
			)
		));
    }
}
