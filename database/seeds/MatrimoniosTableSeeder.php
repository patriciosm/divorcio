<?php

use Illuminate\Database\Seeder;

class MatrimoniosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('matrimonios')->insert(array(
                        array(
                        	'idUser' => 1,
                        	'fechaMatrimonio' => '2011-08-16' ,
                        	'ceseConvivencia' => '2016-10-10' ,
                        	'dejaHogar' => 'Marido' ,
                        	'renunciaCompensacion' => 1 ,
                        	'favorCompensacion' => '' ,
                        	'montoCompensacion' => 0 ,
                            'tieneBienes' => 1 ,
                        	'nombresMarido' => 'Juan Carlos' ,
                        	'apellidosMarido' => 'Muñoz Acevedo' ,
                        	'rutMarido' => '14.167.790-K' ,
                        	'ocupacionMarido' => 'Ingeniero' ,
                            'domicilioMarido' => 'Chaura 1204' ,
                        	'idComunaMarido' => 90 ,
                        	'emailMarido' => '' ,
                        	'telefonoMarido' => '' ,
                        	'nombresMujer' => 'Andrea Carolina' ,
                        	'apellidosMujer' => 'Cárdenas Vásquez' ,
                        	'rutMujer' => '16.756.879-6',
                            'ocupacionMujer' => 'Actriz' ,
                            'domicilioMujer' => 'Padre Hurtado 3838' ,
                        	'idComunaMujer' => 145 ,
                        	'emailMujer' => '' ,
                        	'telefonoMujer' => '' ,
                        	'cantidadHijos' => 2 ,
                        	'montoPension' => 3000000 ,
                            'estado' => 0 ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s") 
                        	) ,
                        array(
                        	'idUser' => 2,
                        	'fechaMatrimonio' => '2015-10-11' ,
                        	'ceseConvivencia' => '2016-10-09' ,
                        	'dejaHogar' => 'Mujer' ,
                        	'renunciaCompensacion' => 0 ,
                        	'favorCompensacion' => 'Mujer' ,
                        	'montoCompensacion' => 1500000 ,
                            'tieneBienes' => null ,
                        	'nombresMarido' => 'Cristian Alberto' ,
                        	'apellidosMarido' => 'Rojas Escobar' ,
                        	'rutMarido' => '12.736.874-5' ,
                        	'ocupacionMarido' => 'Músico' ,
                            'domicilioMarido' => 'Pedro Aguirre Cerda 3487' ,
                        	'idComunaMarido' => 313 ,
                        	'emailMarido' => 'cristian.rescobar@gmail.com' ,
                        	'telefonoMarido' => '' ,
                        	'nombresMujer' => 'Natalia Andrea' ,
                        	'apellidosMujer' => 'González Hevia' ,
                        	'rutMujer' => '13.435.223-7',
                        	'ocupacionMujer' => 'Dueña de casa' ,
                            'domicilioMujer' => 'Monte Aconcagua 1234' ,
                        	'idComunaMujer' => 321 ,
                        	'emailMujer' => '' ,
                        	'telefonoMujer' => '923443912' ,
                        	'cantidadHijos' => 0 ,
                        	'montoPension' =>  0 ,
                            'estado' => 1 ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        	) 
        ));
    }
}
