<?php

use Illuminate\Database\Seeder;

class RegionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regiones')->insert(array(
                        array("region" => "I de Tarapacá") ,
                        array("region" => "II de Antofagasta") ,
                        array("region" => "III de Atacama") ,
                        array("region" => "IV de Coquimbo") ,
                        array("region" => "V de Valparaíso") ,
                        array("region" => "VI de O'Higgins") ,
                        array("region" => "VII del Maule") ,
                        array("region" => "VIII del Bío-Bío") ,
                        array("region" => "IX La Araucanía") ,
                        array("region" => "X de Los Lagos") ,
                        array("region" => "XI Aysén") ,
                        array("region" => "XII de Magallanes") ,
                        array("region" => "Región Metropolitana") ,  
                        array("region" => "XIV de Los Ríos") ,
                        array("region" => "XV de Arica y P.") ,
                        array("region" => "XVI de Ñuble") ,
        ));
    }
}
