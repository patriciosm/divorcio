<?php

use Illuminate\Database\Seeder;

class TiposvisitasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tiposvisitas')->insert(array(	
        	array('tipo' => 'Todos los fines de semana') ,
			array('tipo' => 'Fines de semana por medio') ,
			array('tipo' => 'Solo los domingos') ,
			array('tipo' => 'Solo los sábados') ,
			array('tipo' => 'Otros')
		));
    }
}
