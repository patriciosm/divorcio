<?php

use Illuminate\Database\Seeder;

class HijosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hijos')->insert(array(
                        array(
                        	'idMatrimonio' => 1 ,
                        	'nombres' => 'Jose Luis' ,
                        	'apellidos' => 'Muñoz Cárdenas' ,
                        	'edad' => 24 ,
                        	'rut' => '17839645-5' ,
                        	'idOcupacion' => 1 ,
                        	'idTuicion' => 2 ,
                        	'lugar' => '' ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        	) ,
                        array(
                        	'idMatrimonio' => 1 ,
                        	'nombres' => 'Claudia Andrea' ,
                        	'apellidos' => 'Muñoz Cárdenas' ,
                        	'edad' => 30 ,
                        	'rut' => '15132346-K' ,
                        	'idOcupacion' => 2 ,
                        	'idTuicion' => 2 ,
                        	'lugar' => '' ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        	)
        ));
    }
}
