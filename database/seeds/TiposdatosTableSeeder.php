<?php

use Illuminate\Database\Seeder;

class TiposdatosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tiposdatos')->insert(array(
                        array('tipo' => 'Principal') ,
                        array('tipo' => 'Secundario') ,
                        array('tipo' => 'Caluga') 
        ));
    }
}
