<?php

use Illuminate\Database\Seeder;

class AbogadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abogados')->insert(array(
                        array('nombre' => 'Daniela' , 'apellido' => 'Salina') ,
                        array('nombre' => 'Felipe' , 'apellido' => 'Diaz') 
        ));
    }
}
