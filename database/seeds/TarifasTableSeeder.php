<?php

use Illuminate\Database\Seeder;

class TarifasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tarifas')->insert(array(
                        array(
                        	'titulo' => 'Divorcio mutuo acuerdo plan 1' ,
                        	'texto' => 'Divorcio mutuo acuerdo, sin bienes entre los conyuges.' ,
                        	'valor' => 300000 ,
                            'created_at' => date("Y-m-d H:i:s") ,
                            'updated_at' => date("Y-m-d H:i:s")
                        ),
                        
                        array(
                        	'titulo' => 'Divorcio mutuo acuerdo plan 2' ,
                        	'texto' => 'Divorcio mutuo acuerdo, con bienes que liquidar.' ,
                        	'valor' => 400000 ,
                            'created_at' => date("Y-m-d H:i:s") ,
                            'updated_at' => date("Y-m-d H:i:s")
                        ),
                        
                        array(
                        	'titulo' => 'Divorcio mutuo acuerdo plan 3' ,
                        	'texto' => 'Divorcios unilaterales sin acuerdos entre las partes.' ,
                        	'valor' => 500000 ,
                            'created_at' => date("Y-m-d H:i:s") ,
                            'updated_at' => date("Y-m-d H:i:s")
                        )
        ));
    }
}
