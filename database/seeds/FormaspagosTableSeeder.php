<?php

use Illuminate\Database\Seeder;

class FormaspagosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formaspagos')->insert(array(
                        array('formapago' => 'Efectivo') ,
                        array('formapago' => 'Transferencia') ,
                        array('formapago' => 'Webpay') 
        ));
    }
}
