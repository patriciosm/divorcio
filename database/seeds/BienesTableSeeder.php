<?php

use Illuminate\Database\Seeder;

class BienesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bienes')->insert(array(
                        array(
                        	'idMatrimonio' => 1 ,
                        	'nombre' => 'Casa en la playa' ,
                        	'direccion' => 'Quinteros' ,
                        	'valor' => 54000000 ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        	) ,
                        array(
                        	'idMatrimonio' => 1 ,
                        	'nombre' => 'Automóvil' ,
                        	'direccion' => '' ,
                        	'valor' => 5100000 ,
                        	'created_at' => date("Y-m-d H:i:s") ,
                        	'updated_at' => date("Y-m-d H:i:s")
                        	) 
        ));
    }
}
