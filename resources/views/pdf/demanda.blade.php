<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Prototipo de demanda - Divorcio en linea</title>
	</head>
	<body>
		<div>
			<div>
				<table>
					<tbody>
						<tr>
							<td>MATERIA</td>
							<td>DIVORCIO DE COMUN ACUERDO.</td>
						</tr>
						<tr>
							<td>DEMANDANTE</td>
							<td>{{ strtoupper($data->nombresMarido . " " . $data->apellidosMarido) }}</td>
						</tr>
						<tr>
							<td>RUT</td>
							<td>{{ $data->rutMarido }}</td>
						</tr>
						<tr>
							<td>DOMICILIO</td>
							<td>{{ strtoupper($data->domicilioMarido) }}</td>
						</tr>
						<tr>
							<td>ABOGADO</td>
							<td> NANCY DANIELA SALINA REYES </td>
						</tr>
						<tr>
							<td>RUT</td>
							<td> 15.941.958-4 </td>
						</tr>
						<tr>
							<td>EMAIL</td>
							<td> SOCIEDADCHILE@GMAIL.COM </td>
						</tr>
						<tr>
							<td>DEMANDADO</td>
							<td>{{ strtoupper($data->nombresMujer . " " . $data->apellidosMujer) }}</td>
						</tr>
						<tr>
							<td>RUT</td>
							<td>{{ $data->rutMujer }}</td>
						</tr>
						<tr>
							<td>DOMICILIO</td>
							<td>{{ ($data->domicilioMujer) }}</td>
						</tr>
						<tr>
							<td>ABOGADO</td>
							<td> MARISOL BEATRIZ MORENO ULLOA </td>
						</tr>
						<tr>
							<td>RUT</td>
							<td> 17.104.227-5 </td>
						</tr>
						<tr>
							<td>EMAIL</td>
							<td> SOCIEDADCHILE@GMAIL.COM </td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div>
			<div>
				<p><strong>En lo principal</strong>, se declare divorcio de común acuerdo; <strong>en el primer otrosi</strong>, acuerdo Completo y suficiente; <strong>en el segundo otrosí</strong>; documentos,  <strong>en el tercer otrosí</strong>,   patrocinio y poder.- <strong>en el cuarto otrosí</strong>, patrocinio y poder</p>
			</div>
			<h3>S.J.L. DE FAMILIA</h3>
			<div>
				<p>{{ strtoupper($data->nombresMarido . " " . $data->apellidosMarido) }}, chileno(a), {{ $data->ocupacionMarido }}, casada(a), cédula de identidad número {{ $data->rutMarido }} con domicilio en calle {{ $data->domicilioMarido }} comuna de {{ $data->comunaMarido->comuna }}, de paso por esta, ciudad de Santiago y don(ña) {{ strtoupper($data->nombresMujer . " " . $data->apellidosMujer) }}, chileno, {{ $data->ocupacionMujer }}, casado(a), cédula de identidad número {{ $data->rutMujer }} con domicilio en {{ $data->domicilioMujer }} comuna de {{ $data->comunaMujer->comuna }}, de paso por esta, ciudad de Santiago a US. Con respeto decimos:</p>
				<p>Que venimos en solicitar se declare el divorcio vincular de nuestro matrimonio  celebrado el día {{ $data->fechaMatrimonio }}, ante el Oficial de Registro Civil, el que se inscribió en el Registro de Matrimonios, en atención a los siguientes fundamentos:</p>
				<p><strong>PRIMERO:</strong> Los comparecientes nos encontramos separados  de hecho desde el {{ $data->ceseConvivencia }}, día en que convenimos separarnos definitivamente, al encontrarnos de acuerdo en la imposibilidad de continuar  la vida en común por los diversos problemas conyugales que teníamos.-</p>
				<p><strong>SEGUNDO:</strong> Al encontrarnos separados de hecho hace once años, por lo cual, se configura la causal de divorcio del artículo 55 inciso primero de la Ley de Matrimonio Civil, por haber cesado la convivencia conyugal por ya más de un año.-</p>
				<p><strong>TERCERO:</strong> Como información  adicional señalamos a Us. Que  del referido matrimonio nacieron {{ $data->cantidadHijos }} hijos.</p>
				<p><strong>CUARTO:</strong> Los comparecientes hemos conversado sobre la necesidad de obtener el divorcio, que la separación es irreversible y por ello la disolución del vínculo, por medio del divorcio, sólo regulara en derecho, en la forma que estimamos justa, la situación de hecho en que nos encontramos viviendo actualmente.</p>
				<p>Por tanto, conforme al texto actual de la Ley de Matrimonio Civil,</p>
				<h3>POR TANTO;</h3>
				<p>ROGAMOS A SS.: Tener por interpuesta  solicitud de  divorcio vincular de matrimonio celebrado entre los comparecientes don(ña) {{ strtoupper($data->nombresMarido . " " . $data->apellidosMarido) }} y don(ña) {{ ($data->nombresMujer . " " . $data->apellidosMujer) }}, en definitiva acogerla, declarando ese divorcio y disuelto por ello el matrimonio.</p>
				<p><strong>PRIMER OTROSI:</strong> En virtud de lo expuesto en los párrafos precedentes los cónyuges vienen en regular las materias que se indican en el artículo 21 de la Ley de Matrimonio Civil  en los siguientes términos:</p>
				<p><strong>1.-Respecto al régimen patrimonial del matrimonio,</strong> En cuanto al régimen de bienes del matrimonio, las declaran que {{ $data->tienenBienes }} tienen bienes en común. </p>
				
				@if( $data->renunciaCompensacion == 1 )
				<p><strong>2.-Respecto a  compensaciones económicas,</strong> Que, los cónyuges declaran y expresan su renuncia a demandarse mutuamente cualquier suma por concepto de compensación económica que pudiera originarse con motivo de la disolución del matrimonio, no habiendo sufrido ninguno de los cónyuges el menoscabo económico a que se refiere el artículo sesenta y uno de la Ley diecinueve mil novecientos cuarenta y siete de Matrimonio Civil.</p>
				@endif

				<p>En virtud de lo expuesto en los párrafos precedentes  no existe pendiente entre los cónyuges ninguna de las materias que se indican en el artículo 21 de la Ley de Matrimonio Civil.</p>
				<p><strong>SEGUNDO OTROSI:</strong> Rogamos a Us. Tener por acompañados, los siguientes documentos:</p>
				<ol>
					<li>Certificado de matrimonio de los comparecientes.</li>
					<li>Acta de acuerdos derivados del cese de la convivencia.</li>
					<li>Mandato judicial de don(ña) {{ strtoupper($data->nombresMarido . " " . $data->apellidosMarido) }} a don(ña) NANCY DANIELA SALINA REYES.</li>
					<li>Mandato judicial de don(ña) {{ strtoupper($data->nombresMujer . " " . $data->apellidosMujer) }} a don(ña) MARISOL BEATRIZ MORENO ULLOA </li>
				</ol>
				<p><strong>TERCER OTROSI:</strong> Por este acto  la compareciente {{ strtoupper($data->nombresMarido . " " . $data->apellidosMarido) }}, viene en  designar abogado y apoderado a don(ña) NANCY DANIELA SALINA REYES,  abogado habilitado, cédula de identidad 15.941.958-4, domiciliada en Compañía de Jesús 1390, oficina 2403, comuna y ciudad de Santiago, a quien le otorga  todas y cada una de las facultades  especiales señaladas en el inciso primero y  segundo del artículo  7 del C. de P. Civil, las que se dan  una a una por reproducidas.-</p>
				<p><strong>CUARTO OTROSI:</strong> Por este acto el compareciente {{ strtoupper($data->nombresMujer . " " . $data->apellidosMujer) }},  viene en  designar abogado y apoderado a don(ña) MARISOL BEATRIZ MORENO ULLOA, abogada habilitada, cédula de identidad 17.104.227-5, domiciliada en Compañía de Jesús 1390, oficina 2403, comuna y ciudad de Santiago,  a quien le otorga  todas y cada una de las facultades  especiales señalada en el inciso primero y  segundo del artículo  7 del C. de P. Civil, las que se dan  una a una por reproducidas.-</p>
			</div>
		</div>
	</body>
</html>


	