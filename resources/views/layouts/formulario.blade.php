@extends('layouts.master-users')

@section('contenido')
<div class="fondo-invierno fila">
	<div class="contenedor">
		<div class="texto-linea-lateral">
			<div class="texto-lateral">
				<h2>Formulario	<span class="subtitulo">de Divorcio por mutuo acuerdo</span></h2>
			</div>
			<div class="linea-lateral-gris"></div>
		</div>
		<p class="texto-naranjo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris rutrum justo id sapien efficitur faucibus. Duis id tortor finibus, venenatis massa sed, porta leo.</p>

		<div class="menu-formulario">
			<a href="#" class="btn-menu-form btn-naranjo btn-chico">Menu Formulario</a>
			<ul>
			<?php
				$menuFormulario = array(
									array('ruta' => 'form.matrimonio' , 'enlace' => 'Matrimonio' , 'icono' => 'icon-matrimonio'),
									array('ruta' => 'form.conyuges' , 'enlace' => 'Conyuges' , 'icono' => 'icon-conyuges'),
									array('ruta' => 'form.hijos' , 'enlace' => 'Hijos' , 'icono' => 'icon-personas'),
									array('ruta' => 'form.bienes' , 'enlace' => 'Bienes' , 'icono' => 'icon-carpeta'),
									array('ruta' => 'form.visitas' , 'enlace' => 'Visitas' , 'icono' => 'icon-visitas')
									);
				$numero = count($menuFormulario);
			?>
				@for( $i = 0; $i < $numero ; $i++ )
					<li>
						<a  href="{{ route($menuFormulario[$i]['ruta']) }}" class="item-menu-form 
						@if( Route::currentRouteName() === $menuFormulario[$i]['ruta'] )
							item-menu-activo
						@endif">
							<span class='contenedor-icono'>
								<span class="{{ $menuFormulario[$i]['icono'] }}"></span>
							</span>{{ $menuFormulario[$i]['enlace'] }}
						</a>
					</li>
				@endfor
			</ul>
		</div>
		<div class="contenedor-formulario-divorcio">
			@yield('form_paso')
		</div>
	</div>
</div>
@stop
@section('scripts')
	@parent
	<script src="{{ asset('js/funciones-users.js') }}"></script>
@endsection
