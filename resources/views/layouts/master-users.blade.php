<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <!-- CSRF Token -->
	    <meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield('titulo') - Divorcio en línea</title>

		@yield('seo')
		
		<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.css">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/fontello.css') }}">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="{{ asset('css/estilos.css') }}">
	</head>
	
	<body>
		<header>
			<div class="fondo-higo" id="arriba">
				<div class="contenedor">
						<div class="acceso">
						@if(Auth::check())
							<a href="{{ route('dashboard') }}">{{ Auth::user()->email }}</a>
							<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="icon-ingreso"></span>Cerrar</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						@else

							<a href="#" class="btn-registro"><span class="icon-registro"></span>Registro</a>
							<a href="#" class="btn-sesion"><span class="icon-ingreso"></span>Ingreso</a>
						
						@endif
						</div>
				</div>
			</div>
			<div class="fondo-menu">
				<div class="contenedor">
					@if(Route::currentRouteName() == "home")
						<h1 class="logo"><a href="{{ route('home') }}" title="divorcio en linea">Divorcio en linea</a></h1>
						<div class="boton-menu">
							<a href="#"><span class="icon-menu">Menu</span></a>
						</div>
						<nav>
							<div class="boton-menu">
								<a href="#"><span class="icon-cancel-1"></span>Cerrar</a>
							</div>
							<ul class="menu">
								<li>
									<a href="{{ route('home') }}/#como-funciona">¿Cómo funciona?</a>
								</li>
								<li>
									<a href="{{ route('home') }}/#servicio">Servicio</a>
								</li>
								<li>
									<a href="{{ route('home') }}/#conoce-mas">Conoce más</a>
								</li>
								<li>
									<a href="{{ route('home') }}/#blog">Blog</a>
								</li>
								<li>
									<a href="{{ route('home') }}/#pagos">Pagos</a>
								</li>
							</ul>
						</nav>
					@else
						<div class="logo"><a href="{{ route('home') }}" title="divorcio en linea">Divorcio en linea</a></div>
						<div class="nav">
							<ul>
								<li><a href="{{ route('home') }}">Home</a></li>
							</ul>
						</div>
					@endif
				</div>
			</div>
		</header>
		<div role="main" class="cuerpo">
			@yield('contenido')
		</div>
		<footer class="fondo-higo">
			<div class="fila">
				<div class="contenedor">
					<div class="logo"><a href="{{ route('home') }}" title="divorcio en linea">Divorcio en línea</a></div>
					<p>Dirección: Compañía 1390 oficina 2104, Santiago Centro, metro Santa Ana. Edificio YMCA</p>
					<p>Contacto: 232029700</p>
					<div class="redes-sociales">
						<a href="#"><span class="icon-facebook"></span></a>
						<a href="#"><span class="icon-twitter"></span></a>
						<a href="#"><span class="icon-linkedin"></span></a>
					</div>
				</div>
			</div>
		</footer>
		@section('scripts')
			<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
		@show	
	</body>
</html>