<script src="{{ asset('js/tinymce.min.js') }}"></script>
	<script type="text/javascript">
	tinymce.init({
	    selector: "textarea",
	    style_formats: [
	    	{title:'Texto Naranjo' , selector:'p' , classes:'texto-naranjo'},
	    	{title:'Fuente Alternativa' , selector:'p' , classes:'fuente-alternativa'}
	    ],
	    plugins: [
	        "advlist autolink lists link image charmap print preview",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime media table contextmenu paste"
	    ],
	    menubar:"insert view format table",
	    toolbar: "insertfile undo redo | styleselect | bold italic | " +
	             "alignleft aligncenter alignright alignjustify | " +
	             "bullist numlist outdent indent | link image"
	});
</script>