@extends('layouts.administrador')

@section('titulo')
	Ver caso
@endsection

@section('contenido')

{{-- 
En CasoController se crea una instancia del modelo Caso con los datos correspondientes al id, los cuales 
son vaciados en esta vista, llamando a metodos como matrimonio / abogado / pagos / estado / horasTomadas / 
horasAudiencias (todos del modelo Caso), comunaMarido y comunaMujer (modelo Matrimonio), region (modelo Comuna).
--}}

	<h1>Caso</h1>
	<h3>Datos del Matrimonio</h3>
	<div class="row">
		<div class="col-md-12">

			<div>Fecha matrimonio: {{ formatoFecha($caso->matrimonio->fechaMatrimonio) }}</div>
			<div>Fecha cese de convivencia: {{ formatoFecha($caso->matrimonio->ceseConvivencia) }}</div>
			<div>Quien dejo el hogar: {{ $caso->matrimonio->dejaHogar }}</div>
			@if($caso->matrimonio->renunciaCompensacion)
				<div>Renuncia a la compensación: Si</div>
			@else
				<div>Renuncia a la compensación: No</div>
				<div>A favor de quien es la compensación: {{ $caso->matrimonio->favorCompensacion }}</div>
				<div>Monto compensación: {{ formatoMiles($caso->matrimonio->montoCompensacion) }}</div>
			@endif		

			<h4>Marido</h4>
			<div>
				<div>RUT:{{ $caso->matrimonio->rutMarido }}</div>
				<div>Nombres: {{ $caso->matrimonio->nombresMarido }}</div>
				<div>Apellidos: {{ $caso->matrimonio->apellidosMarido }}</div>
				<div>Ocupación: {{ $caso->matrimonio->ocupacionMarido }}</div>
				<div>Domicilio: {{ $caso->matrimonio->domicilioMarido }}</div>
				<div>Región: {{ $caso->matrimonio->comunaMarido->region->region }}</div>
				<div>Comuna: {{ $caso->matrimonio->comunaMarido->comuna }}</div>
				<div>Email: {{ $caso->matrimonio->emailMarido }}</div>
				<div>Teléfono: {{ $caso->matrimonio->telefonoMarido }}</div>
			</div>
			<h4>Mujer</h4>
			<div>
				<div>RUT:{{ $caso->matrimonio->rutMujer }}</div>
				<div>Nombres: {{ $caso->matrimonio->nombresMujer }}</div>
				<div>Apellidos: {{ $caso->matrimonio->apellidosMujer }}</div>
				<div>Ocupación: {{ $caso->matrimonio->ocupacionMujer }}</div>
				<div>Domicilio: {{ $caso->matrimonio->domicilioMujer }}</div>
				<div>Región: {{ $caso->matrimonio->comunaMujer->region->region }}</div>
				<div>Comuna: {{ $caso->matrimonio->comunaMujer->comuna }}</div>
				<div>Email: {{ $caso->matrimonio->emailMujer }}</div>
				<div>Teléfono: {{ $caso->matrimonio->telefonoMujer }}</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<h4>Bienes</h4>
			@foreach($caso->matrimonio->bienes as $bien)
			<div>
				<div>Nombre: {{ $bien->nombre }}</div>
				<div>Dirección: {{ $bien->direccion }}</div>
				<div>Valor: {{ formatoMiles($bien->valor) }}</div>
			</div>
			@endforeach
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<h4>Hijos</h4>
			<?php $i = 1; ?>
			@foreach($caso->matrimonio->hijos as $hijo)
			<div>
				<div><strong>Hijo {{ $i }}</strong></div>
				<div>Nombres: {{ $hijo->nombres }}</div>
				<div>Apellidos: {{ $hijo->apellidos }}</div>
				<div>Edad: {{ $hijo->edad }}</div>
				<div>Ocupación: {{ $hijo->ocupacion->ocupacion }}</div>
				<div>Tuición: {{ $hijo->tuicion->tutor }}</div>
				<div>Lugar: {{ $hijo->lugar }}</div>
			</div>
			<?php $i++; ?>
			@endforeach
			<div>Monto pensión: {{ formatoMiles($caso->matrimonio->montoPension) }}</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<h4>Visitas</h4>
			<div>Tipo de visita: {{ $caso->matrimonio->visita->tipovisita->tipo or '' }}</div>
			<div>Otro tipo de visita: {{ $caso->matrimonio->visita->otraVisita }}</div>
			<div>Hora de retiro: {{ $caso->matrimonio->visita->horaRetiro }}</div>
			<div>Hora de restitucion: {{ $caso->matrimonio->visita->horaRestitucion }}</div>
			<div>Fecha de inicio las vacaciones de verano: {{ $caso->matrimonio->visita->fechaInicioVerano }}</div>
			<div>Fecha del fin de las vacaciones de verano: {{ $caso->matrimonio->visita->fechaFinVerano }}</div>
			<div>Otra modalidad de vacaciones de verano: {{ $caso->matrimonio->visita->otroVerano }}</div>
			<div>Días de vacaciones de invierno: {{ $caso->matrimonio->visita->diasInvierno }}</div>
			<div>Cumpleaños de los menores alternado, comenzando por: {{ $caso->matrimonio->visita->cumpleanos }}</div>
			<div>Fiestas Patrias, alternado comenzando por: {{ $caso->matrimonio->visita->fiestasPatrias }}</div>
			<div>Cumpleaños de los padres, ¿Pasarán el día con el festejado?: {{ $caso->matrimonio->visita->diaFestejado }}</div>
			<div>En caso que los padres esten de cumpleaños el mismo día, la pasaran alternados comenzando por: {{ $caso->matrimonio->visita->cumplePadres }}</div>
			<div>Día del padre o madre, ¿Pasarán el día con el festejado?: {{ $caso->matrimonio->visita->diaPadreMadre }}</div>
		</div>
	</div>


	<div><a href="#" class="btn btn-primary">Editar matrimonio</a></div>
	<h3>Datos del caso</h3>
	<div class="row">
		<div class="col-md-12">
			<div>Abogado: {{ $caso->abogado->nombreCompleto}}</div>
			<div>Estado: {{ $caso->estado->estado }}</div>
			<h4>Pagos:</h4>
			@foreach($caso->pagos as $pago)
				<div>{{ formatoMiles($pago->valor) ." " . formatoFecha($pago->fecha) ." " . $pago->forma->formapago}}</div>
			@endforeach
			<h4>Horas Tomadas:</h4>
			@foreach($caso->horasTomadas as $horaTomada)
				<div>{{ formatoFecha($horaTomada->fecha) . " " . $horaTomada->hora }}</div>
			@endforeach
				<h4>Horas Audiencias:</h4>
			@foreach($caso->horasAudiencias as $horaAudiencia)
				<div>{{ formatoFecha($horaAudiencia->fecha) . " " . $horaAudiencia->hora }}</div>
			@endforeach
			<div><a href="{{ route('edit-caso' , ['id' => $caso->id]) }}" class="btn btn-primary">Editar caso</a></div>
		</div>
	</div>
@endsection