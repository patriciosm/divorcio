@extends('layouts.administrador')

@section('titulo')
	Lstado de casos
@endsection

@section('contenido')
	<div>
		<h2>Listado de Casos</h2>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>RUT Marido</th>
					<th>RUT Mujer</th>
					<th>Estado</th>
					<th>Abogado</th>
					<th>Acción</th>
				</tr>	
			</thead>
			<tbody>
				@foreach($casos as $caso)
				<tr>
					<td>{{ $caso->matrimonio->rutMarido }}</td>
					<td>{{ $caso->matrimonio->rutMujer }}</td>
					<td>{{ $caso->estado->estado }}</td>
					<td>{{ $caso->abogado->nombreCompleto }}</td>
					<td><a href="{{ route('ver-caso' , ['id' => $caso->id]) }}">Ver caso</a></td>
				</tr>	
				@endforeach
			</tbody>
		</table>
	</div>
@endsection