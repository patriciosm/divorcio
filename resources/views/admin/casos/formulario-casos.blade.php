@extends('layouts.administrador')

@section('titulo')
	Formulario de edición de casos
@endsection
	
@section('contenido')
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<h1>Formulario de edición de caso</h1>
		{!! Form::open(['route' => 'update-caso']) !!}
		{{ csrf_field() }}
		<div class="form-group">
			{!! Form::label('estado','Estado del caso') !!}
			{!! Form::select('idEstado' , $estados , $caso->idEstado , ['placeholder' => 'Seleccione un estado' ,'class'=>'form-control'] ) !!}
		</div>
		<div class="form-group">
			{!! Form::label('abogado','Abogado del caso') !!}
			{!! Form::select('idAbogado' , $abogados , $caso->idAbogado , ['placeholder' => 'Seleccione un abogado','class'=>'form-control'] ) !!}
		</div>
		{!! Form::hidden('id' , $caso->id) !!}
		{!! Form::submit('Guardar' , array('class'=>'btn btn-primary'))!!}

		{!! Form::close() !!}
	</div>
</div>
@endsection