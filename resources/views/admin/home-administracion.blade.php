@extends('layouts.administrador')

@section('titulo')
	Panel de administracion
@endsection

@section('contenido')
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<h1>Panel de Administración</h1>
		<h3>Divorcio en linea</h3>
		<p>En esta sección usted podrá:</p>
		<ul>
			<li>Editar los contenidos que serán mostrados en el home</li>
			<li>Revisar y editar Casos</li>
		</ul>
		 
		<div class="row">
			<a href="{{ route('lista-contenidos') }}" class="btn btn-info">Contenidos</a>
			<a href="{{ route('lista-casos') }}" class="btn btn-info">Casos</a>
		</div>
	</div>
</div>
@endsection