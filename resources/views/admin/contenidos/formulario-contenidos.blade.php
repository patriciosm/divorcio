@extends('layouts.administrador')

@section('titulo')
	Formulario de edición de contenidos
@endsection

@section('contenido')

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<h1>Formulario de edición de contenidos</h1>
		<form action="{{ route('update-contenido') }}" method="POST" accept-charset="UTF-8">
			{{ csrf_field() }}
			<h4>Sección: <strong>{{ $contenidos->seccion->seccion }}</strong> / Tipo de dato: <strong>{{ $contenidos->tipoDato->tipo }}</strong></h4>
			<div class="form-group">
				<label for="titulo">Título</label>
				<input type="text" name="titulo" class="form-control" value="{{ $contenidos->titulo }}" id="titulo">
			</div>
			
			<div class="form-group">
				<label for="subtitulo">Subtítulo</label>
				@if($contenidos->idTipodato == 3)
					<input type="text" disabled name="subtitulo" class="form-control" value="{{ $contenidos->subtitulo }}" id="subtitulo">
				@else
					<input type="text" name="subtitulo" class="form-control" value="{{ $contenidos->subtitulo }}" id="subtitulo">
				@endif
			</div>
			
			<div class="form-group">
				<label for="texto">Texto</label>
				<textarea name="texto" id="texto" class="form-control">{{ $contenidos->texto }}</textarea>
			</div>
			<input type="hidden" name="id" value="{{ $contenidos->id }}">
			<button type="submit" class="btn btn-primary">Guardar</button>
		</form>
	</div>
</div>

@endsection

@section('scripts')
	@include('admin.partials.tinymce')
@endsection