@extends('layouts.administrador')

@section('titulo')

@endsection

@section('contenido')

	<div>
		<h2>Tabla de contenidos del Home</h2>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>Sección</th>
					<th>Tipo de dato</th>
					<th>Titulo</th>
					<th>Subtitulo</th>
					<th>Texto</th>
					<th>Acción</th>
				</tr>
			</thead>
			<tbody>
				@foreach($contenidos as $contenido)
					<tr>
						<td>{{ $contenido->seccion->seccion }}</td>
						<td>{{ $contenido->tipoDato->tipo }}</td>
						<td>{{ $contenido->titulo }}</td>
						<td>{{ $contenido->subtitulo }}</td>
						<td>{!! $contenido->texto !!}</td>
						<td><a href="{{route('edita-contenido' , ['id' => $contenido->id]) }}">Editar</a></td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<h2>Tarifas</h2>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>Titulo</th>
					<th>Texto</th>
					<th>Valor</th>
					<th>Acción</th>
				</tr>
			</thead>
			<tbody>
				@foreach($tarifas as $tarifa)
					<tr>
						<td>{{ $tarifa->titulo }}</td>
						<td>{{ $tarifa->texto }}</td>
						<td>{{ formatoMiles($tarifa->valor) }}</td>
						<td><a href="{{route('edita-tarifa' , ['id' => $tarifa->id]) }}">Editar</a></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

@endsection