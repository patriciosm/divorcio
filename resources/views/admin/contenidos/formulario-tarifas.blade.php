@extends('layouts.administrador')

@section('titulo')
	Formulario de edición de tarifas
@endsection

@section('contenido')

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<h1>Formulario de edición de tarifas</h1>
		<form action="{{ route('update-tarifa') }}" method="POST" accept-charset="UTF-8">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="titulo">Título</label>
				<input type="text" name="titulo" class="form-control" value="{{ $tarifa->titulo }}" id="titulo">
			</div>
					
			<div class="form-group">
				<label for="texto">Texto</label>
				<input type="text" name="texto" class="form-control" value="{{ $tarifa->texto }}" id="texto">
			</div>

			<div class="form-group">
				<label for="valor">Valor</label>
				<input type="number" name="valor" class="form-control" value="{{ $tarifa->valor }}" id="valor">
			</div>
			<input type="hidden" name="id" value="{{ $tarifa->id }}">
			<button type="submit" class="btn btn-primary">Guardar</button>
		</form>		
	</div>
</div>

@endsection

@section('scripts')
	@include('admin.partials.tinymce')
@endsection