@extends('layouts.master-users')

@section('titulo')
	Home
@endsection

@section('contenido')
	@if(!Auth::check())
		<div class="modal-fondo">
			<div class="modal-contenedor">
				<div class="modal-wrapper">
					<a href="#" class="modal-close"><span class="icon-cancel-1"></span></a>
					<div class="modal-sesion">
						<div class="modal-header">
							<h3>Iniciar sesión</h3>
						</div>
						<div class="modal-body">
							<form action="{{ route('login') }}" method="POST">
								{{ csrf_field() }}
								<div>
									<a href="{{ route('social.auth' , 'facebook') }}" class="btn-form btn-azul btn-facebook">Ingrese con Facebook</a>
								</div>
								<div class="lineas-gemelas">
									<span>o por correo</span>
								</div>
								<div class="form-group">
									<label>Correo electrónico</label>
									<input type="email" name="email" required>
									<strong class="mensaje-error email-error"></strong>
								</div>
								<div class="form-group">
									<label>Contraseña</label>
									<input type="password" name="password" required>
									<strong class="mensaje-error password-error"></strong>
								</div>
								<div class="form-group">
									<strong class="mensaje-error error-datos"></strong>
								</div>
								<div>
									<button type="submit" class="submit-sesion btn-form btn-naranjo">Ingresar</button>
								</div>
							</form>
							<div>
								<a href="#" data-enlace="btn-olvido" class="enlace btn-olvido">¿Olvido su contraseña?</a>
							</div>
						</div>
						<div class="modal-footer">
							¿Eres nuevo? <a href="#" data-enlace="btn-registro" class="enlace btn-registro">Registrate</a>
						</div>
					</div>

					<div class="modal-registro">
						<div class="modal-header">
							<h3>Inscribase</h3>
						</div>
						<div class="modal-body">
							<form action="{{ route('register') }}" method="POST">
								{{ csrf_field() }}
								<div>
									<a href="{{ route('social.auth' , 'facebook') }}" class="btn-form btn-azul btn-facebook">Registrate con Facebook</a>
								</div>
								<div class="lineas-gemelas">
									<span>o por correo</span>
								</div>
								<div class="form-group">
									<label>Nombre</label>
									<input type="text" name="nombre">
									<strong class="mensaje-error nombre-error"></strong>
								</div>
								<div class="form-group">
									<label>Correo electrónico</label>
									<input type="email" name="email">
									<strong class="mensaje-error email-error"></strong>
								</div>
								<div class="form-group">
									<label>Contraseña</label>
									<input type="password" name="password">
									<strong class="mensaje-error password-error"></strong>
								</div>
								<div class="form-group">
									<label>Repita su contraseña</label>
									<input type="password" name="password_confirmation">
									<strong class="mensaje-error password-confirmation-error"></strong>
								</div>
								<div>
									<button type="submit" class="submit-registro btn-form btn-naranjo">Registrese</button>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							¿Ya tienes cuenta? <a href="#" data-enlace="btn-sesion" class="enlace btn-sesion">Ingresar</a>
						</div>
					</div>

					<div class="modal-olvido">
						<div class="modal-header">
							<h3>¿Olvidó su contraseña?</h3>
						</div>
						<div class="modal-body">
							<form action="{{ route('password.email') }}" method="POST">
								{{ csrf_field() }}
								<div class="form-group">
									<label>Correo electrónico</label>
									<input type="email" name="email">
									<strong class="mensaje-error email-error"></strong>
								</div>
								<div>
									<button type="submit" class="submit-olvido btn-form btn-naranjo">Enviar</button>
								</div>
							</form>
							<div class="mensaje-exito"></div>
						</div>
						<div class="modal-footer">
							¿Ya tienes cuenta? <a href="#" data-enlace="btn-sesion" class="enlace btn-sesion">Ingresar</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	@endif

	<section class="ilustracion">
		@if(!Auth::check())
			<div>
				<a href="#" class="btn btn-naranjo btn-lg btn-sesion">Iniciar Sesión</a>
			</div>
		@endif
		<div class="contenedor-slider">
			<div class="contenedor-textos-slider">
				<div class="lista-slider">
					@foreach($contenidos as $contenido)
						@if($contenido->idSeccion == 1)
							<div class="slide">
								<h2>{{ $contenido->titulo }}</h2>
								{!! $contenido->texto !!}
							</div>
						@endif
					@endforeach
				</div>
			</div>
		</div>
	</section>
	<section class="fondo-invierno fila" id="como-funciona">
		<div class="contenedor">
			
			@foreach($contenidos as $contenido)
				@if($contenido->idSeccion == 2 && $contenido->idTipodato == 1)
					<h2>{{ $contenido->titulo }}</h2>
					{!! $contenido->texto !!}
				@endif
			@endforeach

			<div class="contenedor-cajas">

				<?php $i=1; ?>
				@foreach($contenidos as $contenido)
					@if($contenido->idSeccion == 2 && $contenido->idTipodato == 3)
						<div class="paso">
							<span class="{{ $contenido->subtitulo }}"></span>
							<h3>{{ $i }}. {{ $contenido->titulo }}</h3>
							{!! $contenido->texto !!}
						</div>
					<?php $i++; ?>
					@endif
				@endforeach

			</div>

			@foreach($contenidos as $contenido)
				@if($contenido->idSeccion == 2 && $contenido->idTipodato == 2)
					<div class="texto-linea-lateral">
						<div class="texto-naranjo texto-lateral">
							<h2>{{ $contenido->titulo }}<span class="subtitulo">{{ $contenido->subtitulo }}</span></h2>
						</div>
						<div class="linea-lateral-gris"></div>
					</div>
					<div>
						{!! $contenido->texto !!}
					</div>
				@endif
			@endforeach

		</div>
	</section>
	<section class="fondo-morado fila" id="servicio">
		<div class="contenedor">

			@foreach($contenidos as $contenido)
				@if($contenido->idSeccion == 3 && $contenido->idTipodato == 1)
					<h2>{{ $contenido->titulo }}</h2>
					{!! $contenido->texto !!}
				@endif
			@endforeach

			<div class="contenedor-cajas">
				@foreach($contenidos as $contenido)
					@if($contenido->idSeccion == 3 && $contenido->idTipodato == 3)
						<div class="servicio">
							<h3>{{ $contenido->titulo }}</h3>
							{!! $contenido->texto !!}
						</div>
					@endif
				@endforeach
			</div>

		</div>
	</section>
	<section class="fondo-invierno fila" id="conoce-mas">
		<div class="contenedor">

			@foreach($contenidos as $contenido)
				@if($contenido->idSeccion == 4 && $contenido->idTipodato == 1)
					<h2>{{ $contenido->titulo }}</h2>
					{!! $contenido->texto !!}
				@endif
			@endforeach

			<input type="radio" id="etiqueta-1" name="grupo-etiqueta" class="btn-radio" checked/>
			<input type="radio" id="etiqueta-2" name="grupo-etiqueta" class="btn-radio"/>
			<input type="radio" id="etiqueta-3" name="grupo-etiqueta" class="btn-radio"/>
			<input type="radio" id="etiqueta-4" name="grupo-etiqueta" class="btn-radio"/>
			<input type="radio" id="etiqueta-5" name="grupo-etiqueta" class="btn-radio"/>
			
			<div class="navegacion-etiquetas">
				<?php $i=1; ?>
				@foreach($contenidos as $contenido)
					@if($contenido->idSeccion == 4 && $contenido->idTipodato == 3)
						<label for="etiqueta-{{ $i }}" class="nav-etiqueta">{{ $contenido->titulo }}</label>
						<?php $i++; ?>
					@endif
				@endforeach
			</div>

			<div class="contenedor-etiquetas">
				<?php $i=1; ?>
				@foreach($contenidos as $contenido)
					@if($contenido->idSeccion == 4 && $contenido->idTipodato == 3)
						<div class="etiqueta">
							<label for="etiqueta-{{ $i }}" class="titulo-etiqueta">{{ $contenido->titulo }}</label>
							<div class="contenido-etiqueta">
								{!! $contenido->texto !!}
								@if($i == 4)
								<div class="contenedor-tarifas">
									@foreach($tarifas as $tarifa)
										<div class="tarifa">
											<h4 class="tarifa-titulo">{{ $tarifa->titulo }}</h4>
											<p class="tarifa-texto">{{ $tarifa->texto }}</p>
											<p class="tarifa-valor">{{ formatoMiles($tarifa->valor) }}</p>
										</div>
									@endforeach
								</div>
								@endif
							</div>
						</div>
					<?php $i++; ?>
					@endif
				@endforeach		
			</div>		

		</div>
	</section>
	<section class="fondo-naranjo fila" id="blog">
		<div class="contenedor">
			
			@foreach($contenidos as $contenido)
				@if($contenido->idSeccion == 5)
					<div class="texto-linea-lateral">
						<div class="texto-lateral">
							<h2>{{ $contenido->titulo }}<span class="subtitulo">{{ $contenido->subtitulo }}</span></h2>
						</div>
						<div class="linea-lateral-blanca"></div>
					</div>
					<div class="contenedor-columnas">
						<div class="columnas-9">
							{!! $contenido->texto !!}
						</div>
						<div class="columnas-3">
							<a href="#" class="btn btn-naranjo btn-lg">Descubre</a>
						</div>
					</div>
				@endif
			@endforeach
			
		</div>
	</section>
	<section class="fondo-invierno fila" id="pagos">
		<div class="contenedor contenedor-columnas">
			<div class="columnas-9">
				<h3>Medios de pago</h3>
				<ol>
					<li>Efectivo</li>
					<li>Cuotas</li>
					<li>Transferencia electrónica</li>
				</ol>
				<p>Consectetur adipiscing elit. Praesent a dictum ante. Mauris pretium, ut facilisis nec, ultricies quis ante.</p>
			</div>
			<div class="columnas-3 webpay">
				<img src="{{ asset('imagenes/webpay.png') }}" alt="webpay"/>
			</div>
		</div>
	</section>			
@endsection

@section('scripts')
	@parent
	<script type="text/javascript" src="{{ asset('js/funciones.js') }}"></script>
@endsection