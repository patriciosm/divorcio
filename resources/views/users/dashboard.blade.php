@extends('layouts.master-users')

@section('titulo')
	Dashboard
@endsection

@section('contenido')
	<section class="dashboard">
		<div class="fila">
			<div class="contenedor">
				<h2>Informacion General</h2>
				<p>Aca podrá consultar en que se encuentra su proceso de divorcio</p>
				<div class="contenedor-dashboard">
					<div class="caja-dashboard">
						<div class="caja-morada">
							<span class="contenedor-icono"><span class="icon-tramitacion"></span></span>
							<p>Estado de su tramitación</p>
						</div>
						<p>{{ $caso->estado->estado }}</p>
					</div>
					<div class="caja-dashboard">
						<div class="caja-morada">
							<span class="contenedor-icono"><span class="icon-hora"></span></span>
							<p>Horas tomadas</p>
						</div>
						<p>{{ $caso->ultimahoraTomada }}</p>
					</div>
					<div class="caja-dashboard">
						<div class="caja-morada">
							<span class="contenedor-icono"><span class="icon-reembolso"></span></span>
							<p>Pagos</p>
						</div>
						<p>{{ formatoMiles($caso->pagos()->sum('valor')) }}</p>
					</div>
					<div class="caja-dashboard">
						<div class="caja-morada">
							<span class="contenedor-icono"><span class="icon-personas"></span></span>
							<p>Testigos</p>
						</div>
						<ul>
							@foreach($caso->testigos as $testigo)
								<li>{{ $testigo->nombre ." ". $testigo->apellido }}</li>
							@endforeach
						</ul>
						<p>Recuerde informarnos de sus testigos, antes del día de audiencia</p>
					</div>
					<div class="caja-dashboard">
						<div class="caja-morada">
							<span class="contenedor-icono"><span class="icon-persona"></span></span>
							<p>Abogado</p>
						</div>
						<p>{{ $caso->abogado->nombreCompleto }}</p>
					</div>
					<div class="caja-dashboard">
						<div class="caja-morada">
							<span class="contenedor-icono"><span class="icon-hora"></span></span>
							<p>Hora de audiencia</p>
						</div>
						<p>{{ $caso->ultimahoraAudiencia }}</p>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection