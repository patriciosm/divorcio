@extends('layouts.formulario')

@section('titulo')
	Datos del Matrimonio
@endsection

@section('form_paso')
<h3>Listado de  bienes</h3>
<p>Lista de los bienes adquiridos durante el matrimonio</p>
{{ Form::open(['route' => 'form.bienes.save']) }}
	<div class="contenedor-tablas">
		<div class="fila-tabla">
			<span>Tienen bienes adquiridos durante el matrimonio</span> {{ Form::select('tieneBienes' , [0 => 'No' , 1 => 'Si'] , $matrimonio->tieneBienes , ['placeholder' => 'Seleccione']) }}
		</div>
	</div>

@if(empty($bienes))
	<div class="contenedor-campos">
		<h4>Detalle del bien</h4>
		<div class="contenedor-tablas">
			<div class="media-tabla">
				<div class="fila-tabla">
					<label>Nombre</label>
					<div class="celda-tabla">
						{{ Form::text("exp[0][nombre]") }}
					</div>
				</div>
				<div class="fila-tabla">
					<label>Valor $</label>
					<div class="celda-tabla">
						{{ Form::number("exp[0][valor]" , null , ['min' => 0]) }}
					</div>
				</div>
			</div>
			<div class="media-tabla">
				<div class="fila-tabla">
					<label>Dirección</label>
					<div class="celda-tabla">
						{{ Form::text("exp[0][direccion]") }}
					</div>
				</div>
			</div>
		</div>
	</div>

@else

	<?php $i = 0;?>
	@foreach($bienes as $bien)
		<div class="contenedor-campos">
			<h4>Detalle del bien</h4>
			<div class="contenedor-tablas">
				<div class="media-tabla">
					<div class="fila-tabla">
						<label>Nombre</label>
						<div class="celda-tabla">
							{{ Form::text("exp[$i][nombre]" , $bien->nombre) }}
						</div>
					</div>
					<div class="fila-tabla">
						<label>Valor $</label>
						<div class="celda-tabla">
							{{ Form::number("exp[$i][valor]" , $bien->valor , ['min' => 0]) }}
						</div>
					</div>
				</div>
				<div class="media-tabla">
					<div class="fila-tabla">
						<label>Dirección</label>
						<div class="celda-tabla">
							{{ Form::text("exp[$i][direccion]" , $bien->direccion) }}
						</div>
					</div>
				</div>
			</div>
			{{ Form::hidden("exp[$i][id]" , $bien->id) }}
		</div>
		<?php $i++;?>
	@endforeach

@endif

	<div id="contenedor_nuevos_campos"></div>
	<a href="#" class="btn-chico btn-gris" id="agrega">Agregar otro bien</a>

	<div class="botones-formulario">
		<a href="{{ route('form.hijos') }}" class="btn-sm btn-gris"><span>&lt;</span>Atrás</a>
		{{ Form::submit('Guardar y Continuar' , array('class' => 'btn-sm btn-naranjo')) }}
	</div>
{{ Form::close() }}
@endsection