@extends('layouts.formulario')

@section('titulo')
	Paso Final
@endsection

@section('form_paso')
	<h3>Confirmación y envío de datos</h3>
	<p>Para el envío de sus datos y la revisión de su caso por parte de nuestros abogados, deberá presionar enviar.</p>
	{{ Form::open(['route' => 'paso.final.save']) }}
		{{ Form::submit('Enviar' , array('class' => 'btn-chico btn-naranjo')) }}
	{{ Form::close() }}
@endsection