@extends('layouts.formulario')

@section('titulo')
	Datos del Matrimonio
@endsection

@section('form_paso')
<h3>Visitas a los hijos</h3>
{{ Form::open(['route' => 'form.visitas.save']) }}

	<div class="grupo-datos">
		<span>Seleccione el tipo de visita</span>
		{{ Form::select('idTipovisita' , $tiposvisita , $visita->idTipovisita , ['placeholder' => 'Seleccione una opción']) }}
		<div>
			<span>Especifique otro tipo de visita</span>
			{{ Form::text('otraVisita' , $visita->otraVisita) }}
		</div>
	</div>
	<div class="contenedor-flex">
		<div class="columnas-6 grupo-datos">
			<span>hora de retiro de los hijos</span>
			<input type="time" name="horaRetiro" value="{{ $visita->horaRetiro }}">
		</div>
		<div class="columnas-6 grupo-datos">
			<span>Hora de restitución de los hijos</span>
			<input type="time" name="horaRestitucion" value="{{ $visita->horaRestitucion }}">
		</div>
	</div>
	<div class="separador"></div>
	<div>
		<h4>Vacaciones de verano</h4>
		<div class="contenedor-flex">
			<div class="grupo-datos">
				<span>Fecha de inicio</span>
				<input type="date" name="fechaInicioVerano" value="{{ $visita->fechaInicioVerano }}">
			</div>
			<div class="grupo-datos">
				<span>Fecha de término</span>
				<input type="date" name="fechaFinVerano" value="{{ $visita->fechaFinVerano }}">
			</div>
		</div>
		<div class="grupo-datos">
			<span>Otros</span>
			{{ Form::text('otroVerano' , $visita->otroVerano) }}
		</div>
	</div>
	<div>
		<h4>Vacaciones de invierno</h4>
		<div class="grupo-datos">
			<span>Una semana cada uno, cantidad de días</span>
			<input type="number" name="diasInvierno" value="{{ $visita->diasInvierno }}">
		</div>
	</div>
	<div class="separador"></div>
	<div class="grupo-datos">
		<h4>Cumpleaños de los menores</h4>
		<p>Alternado comenzando por el</p>
		{{ Form::select('cumpleanos' , ['Padre' => 'Padre' , 'Madre' => 'Madre'] , $visita->cumpleanos ,['placeholder' => 'Seleccione una opción'] ) }}
	</div>
	<div class="grupo-datos">
		<h4>Fiestas Patrias</h4>
		<span>Alternado comenzando por el</span>
		{{ Form::select('fiestasPatrias' , ['Padre' => 'Padre' , 'Madre' => 'Madre'] , $visita->fiestasPatrias , ['placeholder' => 'Seleccione una opción']) }}
	</div>
	<div class="grupo-datos">
		<h4>Cumpleaños de los padres</h4>
		{{ Form::hidden('diaFestejado' , 0) }}
		{{ Form::checkbox('diaFestejado' , 1 , $visita->diaFestejado == 1 , ['id' => 'diaFestejado' , 'class' => 'btn-radio']) }}
		<label class="label-radio" for="diaFestejado">Pasaran el día con el padre festejado</label>
		<p>En caso que los padres esten de cumpleaños el mismo día, la pasaran alternados comenzando por</p>
		{{ Form::select('cumplePadres' , ['Padre' => 'Padre' , 'Madre' => 'Madre'] , $visita->cumplePadres , ['placeholder' => 'Seleccione una opción']) }}
	</div>
	<div class="grupo-datos">
		<h4>Día del padre o madre</h4>
		{{ Form::hidden('diaPadreMadre' , 0) }}
		{{ Form::checkbox('diaPadreMadre' , 1 , $visita->diaPadreMadre == 1 , ['id' => 'diaPadreMadre' , 'class' => 'btn-radio']) }}
		<label class="label-radio" for="diaPadreMadre">Pasaran el día con el festejado.</label>
	</div>
	<div class="botones-formulario">
		<a href="{{ route('form.bienes') }}" class="btn-sm btn-gris"><span>&lt;</span>Atrás</a>
	{{ Form::submit('Guardar' , array('class' => 'btn-sm btn-naranjo')) }}
	</div>
{{ Form::close() }}
@endsection