@extends('layouts.formulario')

@section('titulo')
	Datos del Matrimonio
@endsection

@section('form_paso')
<h3>Datos de los conyuges</h3>
{{ Form::open(['route' => 'form.conyuges.save']) }}
<div class="contenedor-tabla">
	<div class="media-tabla">
		<h4>Datos del marido</h4>
		<div class="grupo-datos">
			<label>Nombres</label>
			<div class="celda-tabla">
				{{ Form::text('nombresMarido' , $matrimonio->nombresMarido) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Apellidos</label>
			<div class="celda-tabla">
				{{ Form::text('apellidosMarido' , $matrimonio->apellidosMarido) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>RUT</label>
			<div class="celda-tabla">
				{{ Form::text('rutMarido' , $matrimonio->rutMarido , array('maxlength' => 12 , 'id' => 'rutMarido')) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Ocupación</label>
			<div class="celda-tabla">
				{{ Form::text('ocupacionMarido' , $matrimonio->ocupacionMarido) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Domicilio</label>
			<div class="celda-tabla">
				{{ Form::text('domicilioMarido' , $matrimonio->domicilioMarido) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Región</label>
			<div class="celda-tabla">
				{{ Form::select('regionMarido' , $regiones , $matrimonio->comunaMarido->idRegion , ['placeholder' => 'Seleccione región' , 'class' => 'region']) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Comuna</label>
			<div class="celda-tabla">
				{{ Form::select('idComunaMarido' , $comunas , $matrimonio->idComunaMarido , ['placeholder' => 'Seleccione comuna' , 'class' => 'comuna']) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Email</label>
			<div class="celda-tabla">
				<input type="email" name="emailMarido" value="{{ $matrimonio->emailMarido }}">
			</div>
		</div>
		<div class="grupo-datos">
			<label>Telefono</label>
			<div class="celda-tabla">
				{{ Form::text('telefonoMarido' , $matrimonio->telefonoMarido , array('maxlength' => '10')) }}
			</div>
		</div>
	</div>
	<div class="media-tabla">
		<h4>Datos de la mujer</h4>
		<div class="grupo-datos">
			<label>Nombres</label>
			<div class="celda-tabla">
				{{ Form::text('nombresMujer' , $matrimonio->nombresMujer) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Apellidos</label>
			<div class="celda-tabla">
				{{ Form::text('apellidosMujer' , $matrimonio->apellidosMujer) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>RUT</label>
			<div class="celda-tabla">
				{{ Form::text('rutMujer' , $matrimonio->rutMujer , array('maxlength' => 12 , 'id' => 'rutMujer')) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Ocupación</label>
			<div class="celda-tabla">
				{{ Form::text('ocupacionMujer' , $matrimonio->ocupacionMujer) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Domicilio</label>
			<div class="celda-tabla">
				{{ Form::text('domicilioMujer' , $matrimonio->domicilioMujer) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Región</label>
			<div class="celda-tabla">
				{{ Form::select('regionMujer' , $regiones , $matrimonio->comunaMujer->idRegion , ['placeholder' => 'Seleccione región' , 'class' => 'region']) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Comuna</label>
			<div class="celda-tabla">
				{{ Form::select('idComunaMujer' , $comunas , $matrimonio->idComunaMujer , array('placeholder' => 'Seleccione comuna' , 'class' => 'comuna')) }}
			</div>
		</div>
		<div class="grupo-datos">
			<label>Email</label>
			<div class="celda-tabla">
				<input type="email" name="emailMujer" value="{{ $matrimonio->emailMujer }}">
			</div>
		</div>
		<div class="grupo-datos">
			<label>Telefono</label>
			<div class="celda-tabla">
				{{ Form::text('telefonoMujer' , $matrimonio->telefonoMujer , array('maxlength' => '10')) }}
			</div>
		</div>
	</div>
</div>
<div>
	<div class="grupo-opciones">
		<div>
			<label>¿Cuantos hijos en común tienen?</label>
			{{ Form::number('cantidadHijos' , $matrimonio->cantidadHijos , ['min' => 0 , 'max' => 20]) }}
		</div>
	</div>
</div>
<div class="botones-formulario">
	<a href="{{ route('form.matrimonio') }}" class="btn btn-sm btn-gris"><span>&lt;</span>Atrás</a>
	{{ Form::submit('Guardar y Continuar' , array('class' => 'btn btn-sm btn-naranjo')) }}
</div>
{{ Form::close() }}
@endsection

@section('scripts')
	@parent
	<script src="{{ asset('js/valida_rut.js') }}"></script>
	<script>
		// $(document).on('ready' , function()
		// {
		// 	$('.activa-hijos').mostrardatos({opcionElegida:'si'});
		// });
	</script>
@endsection