@extends('layouts.formulario')

@section('titulo')
	Mensaje Final
@endsection

@section('form_paso')
	<h3>Felicitaciones</h3>
	<p>Los datos de su matrimonio han sido recibidos, en breve nos pondremos en contacto para agendar una reunión</p>
@endsection