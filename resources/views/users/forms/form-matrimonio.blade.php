@extends('layouts.formulario')

@section('titulo')
	Datos del Matrimonio
@endsection

@section('form_paso')
<h3>Datos del Matrimonio</h3>
{{ Form::open(['route' => 'form.matrimonio.save']) }}
	<div class="grupo-datos">
		<p>¿En qué fecha se contrajo el matrimonio?</p>
		<input type="date" name="fechaMatrimonio" value="{{ $matrimonio->fechaMatrimonio }}" class="text_fecha">
	</div>
	<div class="grupo-datos">
		<p>¿En qué fecha se produjo el cese de convivencia entre los cónyuges?</p>
		<input type="date" name="ceseConvivencia" value="{{ $matrimonio->ceseConvivencia }}" class="text_fecha">
	</div>
	<div class="grupo-datos">
		<p>¿Cuál de los cónyuges dejó el hogar?</p>
		<div class="grupo-radio">
			<div>
				{{ Form::radio('dejaHogar' , 'Marido' , $matrimonio->dejaHogar == 'Marido' , ['id' => 'dejaMarido' , 'class' => 'btn-radio']) }}
				<label for="dejaMarido" class="label-radio">Marido</label>
			</div>
			<div>
				{{ Form::radio('dejaHogar' ,'Mujer' , $matrimonio->dejaHogar == 'Mujer' , ['id' => 'dejaMujer' , 'class' => 'btn-radio']) }}
				<label for="dejaMujer" class="label-radio">Mujer</label>
			</div>
			<div>
				{{ Form::radio('dejaHogar' , 'Ambos' , $matrimonio->dejaHogar == 'Ambos' , ['id' => 'dejaAmbos' , 'class' => 'btn-radio']) }}
				<label for="dejaAmbos" class="label-radio">Ambos</label>
			</div>
		</div>
	</div>
	<div class="grupo-datos">
		<p>¿Ambos cónyuges acuerdan a renunciar a las compensaciones económicas?</p>
		<div class="grupo-radio">
			<div>
				{{ Form::radio('renunciaCompensacion' , 1 , $matrimonio->renunciaCompensacion === 1 , ['id' => 'renunciaSi' , 'class' => 'btn-radio activa-compensacion'] ) }}
				<label for="renunciaSi" class="label-radio">Si</label>
			</div>
			<div>
				{{ Form::radio('renunciaCompensacion' , 0 , $matrimonio->renunciaCompensacion === 0 , ['id' => 'renunciaNo' , 'class' => 'btn-radio activa-compensacion']) }}
				<label for="renunciaNo" class="label-radio">No</label>
			</div>
		</div>
	</div>
	<div class="mostrar-bloque">
		<div class="grupo-datos">
			<p>¿A favor de cuál de los cónyuges se otorga la compensación?</p>
			<div class="grupo-radio">
				<div>
					{{ Form::radio('favorCompensacion' , 'Marido' , $matrimonio->favorCompensacion === 'Marido' , ['id' => 'favorMarido' , 'class' => 'btn-radio']) }}
					<label for="favorMarido" class="label-radio">Marido</label>
				</div>
				<div>
					{{ Form::radio('favorCompensacion' , 'Mujer' , $matrimonio->favorCompensacion === 'Mujer' , ['id' => 'favorMujer' , 'class' => 'btn-radio']) }}
					<label for="favorMujer" class="label-radio">Mujer</label>
				</div>
			</div>
		</div>
		<div class="grupo-datos">
			<p>Monto de la compensación</p>
			$ {{ Form::number('montoCompensacion' , $matrimonio->montoCompensacion) }}
		</div>
	</div>
	<div class="botones-formulario">
		{{ Form::submit('Guardar y Continuar' , array('class' => 'btn-sm btn-naranjo')) }}
	</div>
{{ Form::close() }}
@endsection

@section('scripts')
	@parent
	<script>
		$(document).on('ready' , function()
		{
			$('.activa-compensacion').mostrardatos({opcionElegida:'no'});
		});
	</script>
@endsection