@extends('layouts.formulario')

@section('titulo')
	Datos del Matrimonio
@endsection

@section('form_paso')
<h3>Datos del o de los hijos</h3>
{{ Form::open(['route' => 'form.hijos.save']) }}

	@if(empty($hijos))
		<div class="contenedor-campos">
			<h4>Datos del hijo</h4>
			<div class="contenedor-tablas">
				<div class="media-tabla">
					<div class="fila-tabla">
						<label>Nombres</label>
						<div class="celda-tabla">
							{{ Form::text("exp[1][nombres]") }}
						</div>
					</div>
					<div class="fila-tabla">
						<label>Apellidos</label>
						<div class="celda-tabla">
							{{ Form::text("exp[1][apellidos]") }}
						</div>
					</div>
					<div class="fila-tabla">
						<label>RUT</label>
						<div class="celda-tabla">
							{{ Form::text("exp[1][rut]") }}
						</div>
					</div>
				</div>
				<div class="media-tabla">
					<div class="fila-tabla">
						<label>Edad</label>
						<div class="celda-tabla">
							{{ Form::number("exp[1][edad]" , null , ['min' => 0 , 'max' => 100]) }}
						</div>
					</div>
					<div class="fila-tabla">
						<label>Ocupación</label>
						<div class="celda-tabla">
							{{ Form::select("exp[1][idOcupacion]" , $ocupaciones , null , ['placeholder' => 'Seleccione ocupación']) }}
						</div>
					</div>
				</div>
			</div>
			<div class="grupo-datos">
				<p>¿Con quién vivirá el hijo o tendrá el cuidado personal del hijo?</p>
				<div class="grupo-opciones">
					<div class="grupo-radio">

						@foreach($tuiciones as $tuicion)
							<div>
								{{ Form::radio("exp[1][idTuicion]" , $tuicion->id , null , ['class' => 'btn-radio activa-direccion' , 'id' => $tuicion->tutor]) }}
								<label for="{{ $tuicion->tutor }}" class="label-radio">{{ $tuicion->tutor }}</label>
							</div>
						@endforeach

					</div>
					<div class="mostrar-bloque">
						<label>Dirección</label>
						{{ Form::text("exp[1][lugar]") }}
					</div>
				</div>
			</div>
		</div>
	@else
		<?php $i = 1;?>
		@foreach($hijos as $hijo)
			<div class="contenedor-campos">
				<h4>Datos del hijo</h4>
				<div class="contenedor-tablas">
					<div class="media-tabla">
						<div class="fila-tabla">
							<label>Nombres</label>
							<div class="celda-tabla">
								{{ Form::text("exp[$i][nombres]" , $hijo->nombres) }}
							</div>
						</div>
						<div class="fila-tabla">
							<label>Apellidos</label>
							<div class="celda-tabla">
								{{ Form::text("exp[$i][apellidos]" , $hijo->apellidos) }}
							</div>
						</div>
						<div class="fila-tabla">
							<label>RUT</label>
							<div class="celda-tabla">
								{{ Form::text("exp[$i][rut]" , $hijo->rut) }}
							</div>
						</div>
					</div>
					<div class="media-tabla">
						<div class="fila-tabla">
							<label>Edad</label>
							<div class="celda-tabla">
								{{ Form::number("exp[$i][edad]" , $hijo->edad , array('maxlength' => 2)) }}
							</div>
						</div>
						<div class="fila-tabla">
							<label>Ocupación</label>
							<div class="celda-tabla">
								{{ Form::select("exp[$i][idOcupacion]" , $ocupaciones , $hijo->idOcupacion , ['placeholder' => 'Seleccione ocupación']) }}
							</div>
						</div>
					</div>
				</div>
				<div class="grupo-datos">
					<p>¿Con quién vivirá el hijo o tendrá el cuidado personal del hijo?</p>
					<div class="grupo-opciones">
						<div class="grupo-radio">

							@foreach($tuiciones as $tuicion)
								<div>
									{{ Form::radio("exp[$i][idTuicion]" , $tuicion->id , $hijo->idTuicion == $tuicion->id , ['class' => 'btn-radio activa-direccion' , 'id' => $tuicion->tutor .$i]) }}
									<label for="{{ $tuicion->tutor .$i }}" class="label-radio">{{ $tuicion->tutor }}</label>
								</div>
							@endforeach

						</div>
						<div class="mostrar-bloque">
							<label>Dirección</label>
							{{ Form::text("exp[$i][lugar]" , $hijo->lugar) }}
							{{ Form::hidden("exp[$i][id]" , $hijo->id) }}
						</div>
					</div>
				</div>
			</div>
			<?php $i++;?>
		@endforeach
	@endif

	<div id="contenedor_nuevos_campos"></div>
	<a href="#" class="btn-chico btn-gris" id="agrega">Agregar otro hijo</a>

	<h3>Pensión de alimentos</h3>
	<p>Valor total de pensión, para los hijos menores de 21 años o los menores de 28 años que se encuentren estudiando.</p>
	<div class="grupo-datos">
		$ {{ Form::number('montoPension' , $matrimonio->montoPension) }}
	</div>
	<div class="botones-formulario">
		<a href="{{ route('form.conyuges') }}" class="btn-sm btn-gris"><span>&lt;</span>Atrás</a>
	{{ Form::submit('Guardar y Continuar' , array('class' => 'btn-sm btn-naranjo')) }}
	</div>
{{ Form::close() }}
@endsection

@section('scripts')
	@parent
	<script>
		$(document).on('ready' , function()
		{
			// $('.activa-direccion').mostrardatos({opcionElegida:'otro lugar'});
		});
	</script>
@endsection