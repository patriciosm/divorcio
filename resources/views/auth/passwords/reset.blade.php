@extends('layouts.master-users')

@section('titulo')
	Home
@endsection

@section('contenido')
	<section class="fila">
		<div class="contenedor">
			<div class="form-reset-container">
				<div class="form-reset">
					<div class="form-reset-header">
						<h3>Cambio de contraseña</h3>
					</div>
					<div class="form-reset-body">
						<form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
							{{ csrf_field() }}
							<input type="hidden" name="token" value="{{ $token }}">
							<div>
								<p>Por favor, ingrese su email y una nueva contraseña para su cuenta.</p>
							</div>
							<div class="form-group">
								<label>Correo electrónico</label>
								<input type="email" name="email">
							</div>
							<div><strong class="mensaje-error">{{ $errors->first('email') }}</strong></div>
							<div class="form-group">
								<label>Contraseña</label>
								<input type="password" name="password">
							</div>
							<div><strong class="mensaje-error">{{ $errors->first('password') }}</strong></div>
							<div class="form-group">
								<label>Repita su contraseña</label>
								<input type="password" name="password_confirmation">
							</div>
							<div><strong class="mensaje-error">{{ $errors->first('password_confirmation') }}</strong></div>
							<div>
								<button type="submit" class="btn-form btn-morado">Cambiar contraseña</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection

@section('scripts')
	@parent
@endsection